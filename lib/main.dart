import 'dart:io';

import 'package:eatiano_app/providers/track_provider.dart';
import 'package:eatiano_app/screens/restaurant_screen/reviewScreen.dart';
import 'package:eatiano_app/screens/splash/splash_page.dart';
import 'package:eatiano_app/utils/locator.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import './model/blogs/blogs_provider.dart';
import './model/changeLocation/changeLocation.dart';
import './model/coupon/couponProvider.dart';
import './model/itemDetails/itemDetailsProvider.dart';
import './model/location/location.dart';
import './model/membership/membership.dart';
import './model/popularRestaurants/popularRestaurantProvider.dart';
import 'providers/popular_dishes_provider.dart';
import './model/restaurantProducts/restaurantProductProvider.dart';
import './model/testAPi/testProvider.dart';
import './notificationService/localNotificationService.dart';
import './screens/changeLocation.dart';
import './screens/changePassword.dart';
import './screens/device_location_screen.dart';
import './screens/dishViewAll.dart';
import './screens/forgotPassword.dart';
import './screens/inbox.dart';
import './screens/membership.dart';
import './screens/menu.dart';
import './screens/moms_genie.dart';
import './screens/my_orders/my_orders_page.dart';
import './screens/otp_screen.dart';
import './screens/refer_screen.dart';
import './screens/searchScreen.dart';
import './screens/sign_up.dart';
import './widgets/home/bottom_navigation.dart';
import './widgets/restaurant/postReview.dart';
import 'providers/cart_provider.dart';
import 'providers/order_provider.dart';
import 'providers/login_provider.dart';
import 'repository/profile_provider.dart';
import 'screens/about_us/about_us.dart';
import 'screens/cart/cart_page.dart';
import 'screens/cart/order_details_page.dart';
import 'screens/coupon_page/coupon_page.dart';
import 'screens/home/home.dart';
import 'screens/moms_genie_view_all.dart';
import 'screens/notification/notification_page.dart';
import 'screens/profile/profile_page.dart';
import 'screens/restaurant_screen/restaurant_screen.dart';
import 'screens/reviews/reviews_page.dart';
import 'screens/login/login_page.dart';
import 'screens/wishlist/wishlist_page.dart';

Future<void> backgroundHandler(RemoteMessage message) async {
  print('Firebase Response ${message.data.toString()}');
  print(message.notification!.title);
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await setupLocator();
  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(backgroundHandler);
  LocalNotificationService.initialize();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    HttpOverrides.global = MyHttpOverrides();
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => PopularRestaurantProvider()),
        ChangeNotifierProvider(create: (context) => BlogsProvider()),
        ChangeNotifierProvider(create: (context) => LoginProvider()),
        ChangeNotifierProvider(create: (context) => PopularDishesProvider()),
        ChangeNotifierProvider(create: (context) => CartItemProvider()),
        ChangeNotifierProvider(create: (context) => LocationProvider()),
        ChangeNotifierProvider(create: (context) => MembershipProvider()),
        ChangeNotifierProvider(create: (context) => CouponProvider()),
        ChangeNotifierProvider(create: (context) => RestaurantProductProvider()),
        ChangeNotifierProvider(create: (context) => ProfileProvider()),
        ChangeNotifierProvider(create: (context) => ChangeLocationProvider()),
        ChangeNotifierProvider(create: (context) => OrderProvider()),
        ChangeNotifierProvider(create: (context) => TestProvider()),
        ChangeNotifierProvider(create: (context) => ItemDetailsProvider()),
        ChangeNotifierProvider(create: (context) => TrackingProvider()),
      ],
      builder: (context, child) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          themeMode: ThemeMode.dark,
          theme: ThemeData(
              scaffoldBackgroundColor: const Color.fromRGBO(25, 29, 33, 1),
              primaryColor: const Color.fromRGBO(252, 17, 17, 1),
              appBarTheme: const AppBarTheme(foregroundColor: Colors.white)),
          routes: {
            '/': (context) => const SplashPage(),
            LoginPage.routeName: (context) => const LoginPage(),
            '/sign-up': (context) => SignUp(),
            ForgotPassword.routeName: (context) => const ForgotPassword(),
            ChangePassword.routeName: (context) => const ChangePassword(),
            '/otp-screen': (context) => OTP(),
            '/bottom-bar': (context) => const BottomNavigation(),
            '/home-screen': (context) => const HomeScreen(),
            '/menu-screen': (context) => const Menu(),
            '/profile-screen': (context) => const ProfilePage(),
            '/moms-genie-screen': (context) => const MomsGenie(),
            '/restaurants-screen': (context) => const RestaurantScreen(),
            // '/restaurant-list': (context) => RestaurantList(),
            // '/page-view-screen': (context) => PageViewScreen(),
            '/review-screen': (context) => ReviewScreen(),
            '/post-review': (context) => PostReview(),
            '/inbox-screen': (context) => InboxScreen(),
            '/notification-screen': (context) => NotificationsPage(),
            '/my-order-screen': (context) => const MyOrdersPage(),
            '/reviews-screen': (context) => const ReviewsPage(),
            '/refer-screen': (context) => ReferScreen(),
            '/membership-screen': (context) => MembershipScreen(),
            '/moms-genie-screen-view': (context) => MomsGenieViewAll(),
            '/about-us': (context) => const AboutUs(),
            // '/item-details': (context) => ItemDetails(),
            '/cart-screen': (context) => const CartPage(),
            '/cart-screen-detail': (context) => const OrderDetailsPage(),
            // '/payment-screen': (context) => PaymentScreen(),
            ChangeLocation.routeName: (context) => const ChangeLocation(),
            '/device-location': (context) => DeviceLocationPage(),
            '/search-screen': (context) => const SearchScreen(),
            '/wishlist-screen': (context) => const WishlistPage(),
            '/coupon-screen': (context) => const CouponPage(),
            '/dishview-all': (context) => const DishViewAll()
          },
        );
      },
    );
  }
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)..badCertificateCallback = (X509Certificate cert, String host, int port) => true;
  }
}
