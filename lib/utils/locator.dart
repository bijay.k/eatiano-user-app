import 'package:eatiano_app/api_client/api_client.dart';
import 'package:eatiano_app/repository/internet_connection_service.dart';
import 'package:eatiano_app/repository/location_service.dart';
import 'package:eatiano_app/repository/package_info_service.dart';
import 'package:eatiano_app/utils/session_manager.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:get_it/get_it.dart';

GetIt locator = GetIt.instance;

Future<void> setupLocator() async {
  locator.registerSingleton<SharedPreferences>(await SharedPreferences.getInstance());
  locator.registerLazySingleton<SessionManager>(() => SessionManager(locator.get<SharedPreferences>()));

  locator.registerSingleton<InternetConnectionService>(InternetConnectionImpl());
  locator.registerSingleton<BaseHttpService>(HttpServiceImpl(internetConnection: locator<InternetConnectionService>()));

  locator.registerLazySingleton<LocationService>(() => LocationImpl());
  locator.registerLazySingleton<PackageInfoService>(() => PackageInfoImpl());

}