class Urls {
  /// ====================== PRODUCTION ===========================

  static const String hostName = 'http://13.127.95.127';
  static const String path = '/eatianoBackend/public/index.php';
  // static const String baseUrl = 'https://';

  /// ====================== PRODUCTION ===========================


  /// ====================== INTEGRATION ===========================

  static const String baseUrl = '$hostName$path/api';

/// ====================== INTEGRATION ===========================

  static const String loginUrl = "$baseUrl/auth/login";
  static const String allProductsUrl = "$baseUrl/all_products";
  static const String fetchCouponsUrl = "$baseUrl/auth/coupon";
  static const String ordersUrl = "$baseUrl/auth/order";


  static const String profileUrl = "$baseUrl/auth/me";
  static const String editProfileUrl = "$baseUrl/auth/me";


  static const String allBlogsUrl = '$baseUrl/all_blogs';
  static const String allRestaurantUrl = '$baseUrl/all_restaurant';

  static const String cartListUrl = '$baseUrl/auth/cart';
  static const String couponListUrl = '$baseUrl/auth/coupon';
  static const String generateOrderIdUrl = '$baseUrl/auth/order_id';
  static const String paymentValidationUrl = '$baseUrl/auth/payment_validation';

  static const String deleteWishlistUrl = '$baseUrl/auth/wishlist';
  static const String trackingUrl = '$baseUrl/auth/track_order/';
  static const String logoutUrl = '$baseUrl/auth/logout';


}