import 'package:eatiano_app/utils/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SessionManager {
  final SharedPreferences _pref;
  SessionManager(this._pref);

  Future<bool> clearPref() => _pref.clear();

  String getToken() => _pref.getString(Constants.prefAccessToken) ?? '';
  void setAccessToken(String value) => _pref.setString(Constants.prefAccessToken, value);


}
