import 'package:flutter/material.dart';

class Constants {
  static const String razorpayKey = 'rzp_test_EUeb1ddd88UqGh';

  static const String checkInternetConnection = 'Unable to connect. Please check your Internet Connection';

  static const String prefLoginId = 'pref.login_id';
  static const String prefUserId = 'pref.user_id';
  static const String prefUserName = 'pref.user_name';
  static const String prefUserEmail = 'pref.user_email';
  static const String prefUserMobileNo = 'pref.user_mobile_no';
  static const String prefAlternateContactNo = 'pref.alternate_contact_number';
  static const String prefPassword = 'pref.password';
  static const String prefRoleId = 'pref.role_id';
  static const String prefRoleCode = 'pref.role_code';
  static const String prefRoleName = 'pref.role_name';
  static const String prefIsSupervisor = 'pref.is_supervisor';
  static const String prefAccessToken = 'access_token';


}
const defaultDateFormat = 'yyyy-MM-dd';

const kToastSuccessColor = Color(0xFF8BC34A);
const kToastInfoColor = Color(0xFF2196F3);
const kToastErrorColor = Color(0xFFF44336);
const SUCCESS_RESPONSE_CODE = 200;
