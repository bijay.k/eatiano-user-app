import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../model/blogs/blogs_provider.dart';

class ReviewsPage extends StatefulWidget {
  static const String routeName = '/reviews-screen';

  const ReviewsPage({Key? key}) : super(key: key);

  @override
  ReviewsPageState createState() => ReviewsPageState();
}

class ReviewsPageState extends State<ReviewsPage> {
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      context.read<BlogsProvider>().fetchData();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    final textScale = MediaQuery.of(context).textScaleFactor * 1.2;
    final provider = Provider.of<BlogsProvider>(context).blogs;

    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          leading: InkWell(onTap: () => Navigator.of(context).pushReplacementNamed('/bottom-bar'), child: const Icon(Icons.arrow_back_ios, color: Colors.red)),
          title: Padding(
            padding: EdgeInsets.only(left: width * 0.22),
            child: Text('Reviews', textScaleFactor: textScale, style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
          ),
        ),
        body: Selector<BlogsProvider, bool>(
            selector: (c, p) => p.isLoading,
            builder: (context, _isLoading, child) {
              return _isLoading
                  ? const Center(child: CircularProgressIndicator(color: Colors.red))
                  : ListView.builder(
                      itemBuilder: (context, index) => Container(
                        width: double.infinity,
                        height: height * 0.1,
                        margin: EdgeInsets.only(bottom: height * 0.02),
                        padding: EdgeInsets.only(left: width * 0.05),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                                // _reviews['data'][index]['name']
                                provider['data'][index]['blog_heading'],
                                textScaleFactor: textScale,
                                style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
                            SizedBox(height: height * 0.01),
                            Text(
                                //_reviews['data'][index]['description'],
                                provider['data'][index]['blog_details'],
                                textScaleFactor: textScale,
                                style: const TextStyle(
                                  color: Colors.white,
                                  fontSize: 9,
                                ))
                          ],
                        ),
                      ),
                      itemCount: provider['data'].length,
                    );
            }));
  }
}
