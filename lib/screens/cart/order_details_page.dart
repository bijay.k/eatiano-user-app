import 'package:eatiano_app/model/location/location.dart';
import 'package:eatiano_app/screens/coupon_page/coupon_page.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../providers/cart_provider.dart';
import '../../model/coupon/couponProvider.dart';
import '../payment/payment_page.dart';

class OrderDetailsPage extends StatefulWidget {
  static const String routeName = '/cart-screen-detail';

  const OrderDetailsPage({Key? key}) : super(key: key);

  @override
  OrderDetailsPageState createState() => OrderDetailsPageState();
}

class OrderDetailsPageState extends State<OrderDetailsPage> {
  bool isClicked = false;
  bool isClickedCoupon = false;
  final _key = GlobalKey<FormState>();
  String text = 'Add';

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      Provider.of<CouponProvider>(context, listen: false).fetchCoupons();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    final textScale = MediaQuery.textScalerOf(context).clamp(minScaleFactor: 1.0, maxScaleFactor: 1.2);
    final cartItems = Provider.of<CartItemProvider>(context).cartItems;
    final couponProvider = Provider.of<CouponProvider>(context).discount;
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          elevation: 0,
          centerTitle: true,
          leading: InkWell(onTap: () => Navigator.of(context).pop(), child: const Icon(Icons.arrow_back_ios, color: Colors.red)),
          title: const Text('Order Details', style: TextStyle(color: Colors.white)),
        ),
        body: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.all(15),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // Image.asset(
                  //   cartItems[0].image,
                  //   height: height * 0.12,
                  // ),
                  SizedBox(width: width * 0.02),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(cartItems[0]['restaurant_name'], style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
                      SizedBox(height: height * 0.005),
                      Row(
                        children: [
                          Text(
                            cartItems[0]['rating'] ?? '',
                            textScaler: textScale,
                            style: const TextStyle(color: Colors.red, fontWeight: FontWeight.bold, fontSize: 11),
                          ),
                          SizedBox(width: width * 0.02),
                          Text(
                            '124',
                            textScaler: textScale,
                            style: const TextStyle(color: Colors.grey, fontSize: 11),
                          )
                        ],
                      ),
                      SizedBox(height: height * 0.005),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Burger',
                            textScaler: textScale,
                            style: const TextStyle(color: Colors.grey, fontWeight: FontWeight.bold, fontSize: 12),
                          ),
                          Text(
                            'Continental',
                            textScaler: textScale,
                            style: const TextStyle(color: Colors.grey, fontWeight: FontWeight.bold, fontSize: 12),
                          )
                        ],
                      ),
                      SizedBox(height: height * 0.005),
                      Row(
                        children: [
                          Image.asset('assets/images/location-pin.png'),
                          SizedBox(width: width * 0.02),
                          Selector<LocationProvider, String>(
                            selector: (c, p) => p.address,
                            builder: (context, address, child) {
                              return Text(address, style: const TextStyle(color: Colors.grey, fontWeight: FontWeight.bold, fontSize: 12));
                            },
                          )
                        ],
                      ),
                    ],
                  )
                ],
              ),
            ),
            ListView.builder(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemBuilder: (context, index) => Column(
                children: [
                  Container(
                    width: double.infinity,
                    height: height * 0.06,
                    color: Colors.white,
                    margin: EdgeInsets.only(left: width * 0.04, right: width * 0.04),
                    padding: EdgeInsets.only(left: width * 0.04, right: width * 0.04),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          // '${provider[index].name} x ${provider[index].quantity}',
                          // '${cartItems[index].name} x ${cartItems[index].quantity}',
                          '${cartItems[index]['product_name']} x ${cartItems[index]['quantity']}',
                          textScaler: textScale,
                          style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                        ),
                        Text(
                          '₹ ${(double.parse(cartItems[index]['product_selling_price'])) * double.parse(cartItems[index]['quantity'])}',
                          textScaler: textScale,
                          style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: width * 0.08, right: width * 0.08),
                    child: Divider(height: height * 0.001, thickness: 1, color: Colors.grey),
                  )
                ],
              ),
              itemCount: cartItems.length,
            ),
            SizedBox(height: height * 0.02),
            Container(
              width: double.infinity,
              height: !isClicked ? height * 0.05 : height * 0.15,
              margin: EdgeInsets.only(left: width * 0.04, right: width * 0.04),
              padding: EdgeInsets.only(left: width * 0.04, right: width * 0.04),
              // color: Colors.amber,
              child: !isClicked
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Delivery Instructions',
                          textScaler: textScale,
                          style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 12),
                        ),
                        InkWell(
                            onTap: () => setState(() {
                                  isClicked = !isClicked;
                                }),
                            child: Text(
                              '+ Add Note',
                              textScaler: textScale,
                              style: const TextStyle(color: Colors.red, fontWeight: FontWeight.bold, fontSize: 12),
                            ))
                      ],
                    )
                  : Padding(
                      padding: EdgeInsets.only(top: height * 0.01),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Delivery Instructions',
                                textScaler: textScale,
                                style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 12),
                              ),
                              InkWell(
                                  onTap: () => setState(() {
                                        isClicked = !isClicked;
                                      }),
                                  child: Text(
                                    '- Remove Note',
                                    textScaler: textScale,
                                    style: const TextStyle(color: Colors.red, fontWeight: FontWeight.bold, fontSize: 12),
                                  ))
                            ],
                          ),
                          SizedBox(height: height * 0.02),
                          Form(
                            key: _key,
                            child: Container(
                                height: height * 0.1,
                                width: double.infinity,
                                padding: EdgeInsets.only(left: width * 0.02, top: height * 0.005),
                                decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), border: Border.all(color: Colors.white, width: 2)),
                                child: TextFormField(
                                  keyboardType: TextInputType.multiline,
                                  maxLines: null,
                                  style: const TextStyle(
                                    color: Colors.white,
                                  ),
                                  decoration: const InputDecoration(
                                      // label: Text(
                                      //   'Add Instructions',
                                      //   textScaler: textScale,
                                      //   style: const TextStyle(
                                      //       color: Colors.white,
                                      //       fontWeight: FontWeight.bold),
                                      // ),
                                      border: InputBorder.none),
                                )),
                          ),
                        ],
                      ),
                    ),
            ),
            Padding(
              padding: EdgeInsets.only(left: width * 0.08, right: width * 0.08),
              child: Container(
                width: double.infinity,
                height: height * 0.001,
                margin: EdgeInsets.only(top: height * 0.01, bottom: height * 0.01),
                color: const Color.fromRGBO(161, 218, 46, 1),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: width * 0.08, right: width * 0.08),
              child: Container(
                width: double.infinity,
                height: height * 0.1,
                // color: Colors.amber,
                padding: EdgeInsets.only(top: height * 0.015),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Add Coupon',
                          textScaler: textScale,
                          style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 12),
                        ),
                        InkWell(
                          onTap: () {
                            setState(() {
                              isClickedCoupon = !isClickedCoupon;
                            });
                            if (isClickedCoupon) {
                              text = 'Add';
                              Navigator.of(context)
                                  .pushNamed(CouponPage.routeName, arguments: {'price': context.read<CartItemProvider>().itemAmount.toString()});
                            } else {
                              text = 'Remove';
                              Provider.of<CouponProvider>(context, listen: false).deleteCoupon(couponProvider['code'], couponProvider['amount']);
                            }
                          },
                          child: Text(
                            !isClickedCoupon ? 'Add' : 'Remove',
                            textScaler: textScale,
                            style: const TextStyle(color: Colors.red, fontWeight: FontWeight.bold, fontSize: 12),
                          ),
                        )
                      ],
                    ),
                    !isClickedCoupon
                        ? const SizedBox()
                        : Container(
                            height: height * 0.05,
                            width: double.infinity,
                            padding: EdgeInsets.only(left: width * 0.02, top: height * 0.01),
                            margin: EdgeInsets.only(top: height * 0.01),
                            decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), border: Border.all(color: Colors.white, width: 2)),
                            child: Text(
                              couponProvider['code'] ?? '',
                              textScaler: textScale,
                              style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                            ),
                          )
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: width * 0.08, right: width * 0.08),
              child: Container(
                width: double.infinity,
                height: height * 0.001,
                margin: EdgeInsets.only(top: height * 0.01, bottom: height * 0.01),
                color: const Color.fromRGBO(161, 218, 46, 1),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: width * 0.08, right: width * 0.08),
              child: Container(
                width: double.infinity,
                // height: height * 0.1,
                margin: EdgeInsets.only(top: height * 0.01, bottom: height * 0.01),
                // color: Colors.amber,
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Sub Total',
                          textScaler: textScale,
                          style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 12),
                        ),
                        Text(
                            // '₹ ${totalAmount.toString()}',
                            '₹ ${Provider.of<CartItemProvider>(context).itemAmount.toString()
                            //  - Provider.of<CartItemProvider>(context).deliveryCost
                            }',
                            textScaler: textScale,
                            style: const TextStyle(color: Colors.red, fontWeight: FontWeight.bold, fontSize: 12))
                      ],
                    ),
                    SizedBox(height: height * 0.01),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Delivery Cost', textScaler: textScale, style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 12)),
                        Text('₹ ${Provider.of<CartItemProvider>(context).deliveryCost.toString()}',
                            textScaler: textScale, style: const TextStyle(color: Colors.red, fontWeight: FontWeight.bold, fontSize: 12))
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: width * 0.08, right: width * 0.08),
              child: Container(
                width: double.infinity,
                height: height * 0.001,
                margin: EdgeInsets.only(top: height * 0.01, bottom: height * 0.01),
                color: const Color.fromRGBO(161, 218, 46, 1),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: width * 0.08, right: width * 0.08),
              child: Container(
                margin: EdgeInsets.only(top: height * 0.01, bottom: height * 0.01),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Total', textScaler: textScale, style: const TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 12)),
                    Text('₹ ${Provider.of<CartItemProvider>(context).itemAmount + Provider.of<CartItemProvider>(context).deliveryCost}'.toString(),
                        textScaler: textScale, style: const TextStyle(color: Colors.red, fontWeight: FontWeight.bold, fontSize: 20))
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: width * 0.08, right: width * 0.08),
              child: InkWell(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (_) => PaymentScreen(couponProvider['amount'] ?? 0.0, couponProvider['code'] ?? '')));
                },
                child: Container(
                  width: double.infinity,
                  height: height * 0.06,
                  margin: EdgeInsets.only(top: height * 0.05, bottom: height * 0.05),
                  decoration: BoxDecoration(border: Border.all(color: Colors.white, width: 1), borderRadius: BorderRadius.circular(30)),
                  child: const Center(
                    child: Text('CHECKOUT', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15)),
                  ),
                ),
              ),
            )
          ],
        ));
  }
}
