import 'dart:ui';
import 'package:eatiano_app/model/cart_model.dart';
import 'package:eatiano_app/screens/cart/order_details_page.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../providers/cart_provider.dart';

class CartPage extends StatefulWidget {
  static const String routeName = '/cart-screen';

  const CartPage({Key? key}) : super(key: key);

  @override
  CartPageState createState() => CartPageState();
}

class CartPageState extends State<CartPage> {
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      context.read<CartItemProvider>().fetchCartItems();
    });
    super.initState();
  }

  @override
  void deactivate() {
    context.read<CartItemProvider>().clear();
    super.deactivate();
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    final textScale = MediaQuery.of(context).textScaleFactor * 1.2;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        elevation: 0,
        centerTitle: true,
        leading: InkWell(onTap: () => Navigator.of(context).pop(), child: const Icon(Icons.arrow_back_ios, color: Colors.red)),
        title: const Text('Cart'),
      ),
      body: Selector<CartItemProvider, bool>(
          selector: (c, p) => p.isLoading,
          builder: (context, isLoading, child) {
            return isLoading
                ? const Center(child: CircularProgressIndicator(color: Colors.red))
                : Selector<CartItemProvider, List<CartModel>>(
                    selector: (c, p) => p.cartList,
                    builder: (context, cartList, child) {
                      if(cartList.isEmpty) {
                        return Center(
                          child: Column(
                            children: [
                              Image.asset('assets/images/—Pngtree—sad face 3d render emoji_5870534.png'),
                              const Text('Cart Is Empty', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20))
                            ],
                          ),
                        );
                      }
                      return Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Container(
                                  padding: EdgeInsets.only(left: width * 0.02, top: height * 0.01, right: width * 0.02),
                                  height: height * 0.7,
                                  child: ListView.builder(
                                    itemCount: cartList.length,
                                    itemBuilder: (context, index) {
                                      final cart = cartList.elementAt(index);
                                      return Container(
                                      margin: EdgeInsets.only(bottom: height * 0.02),
                                      padding: EdgeInsets.only(left: width * 0.02, top: height * 0.01, right: width * 0.02),
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Row(
                                            children: [
                                              Flexible(
                                                flex: 1,
                                                fit: FlexFit.loose,
                                                child: Image.network(
                                                  'http://13.127.95.127/eatianoBackend/public${cart.productImage}',
                                                  width: width * 0.22,
                                                  height: height * 0.1,
                                                  errorBuilder: (context, value, stackTrace) => Container(),
                                                ),
                                              ),
                                              SizedBox(width: width * 0.02),
                                              Flexible(
                                                flex: 3,
                                                child: Column(
                                                  children: [
                                                    Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                      children: [
                                                        Text(
                                                          cart.productName ?? '',
                                                          textScaleFactor: textScale,
                                                          style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 14),
                                                        ),
                                                        Row(
                                                          children: [
                                                            Text(
                                                              '₹',
                                                              textScaleFactor: textScale,
                                                              style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18),
                                                            ),
                                                            SizedBox(width: width * 0.005),
                                                            Text(
                                                              (double.parse(cart.productSellingPrice) *
                                                                      double.parse(cart.quantity))
                                                                  .toString(),
                                                              textScaleFactor: textScale,
                                                              style: const TextStyle(color: Colors.red, fontWeight: FontWeight.bold, fontSize: 18),
                                                            )
                                                          ],
                                                        )
                                                      ],
                                                    ),
                                                    SizedBox(height: height * 0.008),
                                                    Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                      children: [
                                                        Row(
                                                          children: [
                                                            Image.asset('assets/images/Path 8561.png'),
                                                            // Text(
                                                            //   cart.rating ?? '',
                                                            //   textScaleFactor: textScale,
                                                            //   style: const TextStyle(color: Colors.red, fontWeight: FontWeight.bold, fontSize: 12),
                                                            // ),
                                                            SizedBox(width: width * 0.01),
                                                            Text(
                                                               // '${cart.totalRatings}',
                                                              '124',
                                                              textScaleFactor: textScale,
                                                              style: const TextStyle(color: Colors.grey, fontWeight: FontWeight.bold, fontSize: 12),
                                                            )
                                                          ],
                                                        ),
                                                        InkWell(
                                                            // onTap: () => Provider.of<
                                                            //             CartItemProvider>(
                                                            //         context,
                                                            //         listen: false)
                                                            //     .deleteItems(
                                                            //         cartProvider[
                                                            //                 index]
                                                            //             .id),
                                                            onTap: () => Provider.of<CartItemProvider>(context, listen: false)
                                                                .deleteCartItems(cart.cartId.toString()),
                                                            child: const Icon(Icons.delete, color: Colors.red)),
                                                      ],
                                                    ),
                                                    SizedBox(height: height * 0.01),
                                                    Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                      children: [
                                                        Container(
                                                          width: width * 0.35,
                                                          height: height * 0.025,
                                                          // color: Colors.red,
                                                          child: Row(
                                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                            children: [
                                                              Text(
                                                                'Burger',
                                                                textScaleFactor: textScale,
                                                                style: const TextStyle(color: Colors.grey, fontWeight: FontWeight.bold, fontSize: 12),
                                                              ),
                                                              Text(
                                                                'Continental',
                                                                textScaleFactor: textScale,
                                                                style: const TextStyle(color: Colors.grey, fontWeight: FontWeight.bold, fontSize: 12),
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                        // InkWell(
                                                        //   onTap: () => Navigator.of(context)
                                                        //       .pushNamed('/cart-screen-detail',
                                                        //           arguments: {
                                                        //         'id': cartProvider[index].id
                                                        //       }),
                                                        //   child: Text(
                                                        //     'Order Details',
                                                        //     textScaleFactor: textScale,
                                                        //     style: const TextStyle(
                                                        //         color: Colors.red,
                                                        //         fontSize: 10),
                                                        //   ),
                                                        // )
                                                      ],
                                                    )
                                                  ],
                                                ),
                                              )
                                            ],
                                          ),
                                          // Flexible(
                                          //   flex: 1,
                                          //   child: Container(
                                          //     width: double.infinity,
                                          //     // color: Colors.blue,
                                          //     child: Center(
                                          //       child: Text(
                                          //         'Order',
                                          //         textScaleFactor: textScale,
                                          //         style: const TextStyle(
                                          //             color: Colors.white,
                                          //             fontWeight: FontWeight.bold,
                                          //             fontSize: 20),
                                          //       ),
                                          //     ),
                                          //   ),
                                          // )
                                        ],
                                      ),
                                    );},
                                    // itemCount: cartProvider['data'].length,
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: width * 0.08, right: width * 0.08),
                                  child: GestureDetector(
                                    onTap: () => Navigator.of(context).pushNamed(OrderDetailsPage.routeName),
                                    child: Container(
                                      width: double.infinity,
                                      height: height * 0.06,
                                      margin: EdgeInsets.only(top: height * 0.05),
                                      decoration: BoxDecoration(border: Border.all(color: Colors.white, width: 1), borderRadius: BorderRadius.circular(30)),
                                      child: const Center(
                                        child: Text('CHECKOUT', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15)),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            );
                    });
          }),
    );
  }
}
