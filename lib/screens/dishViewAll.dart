import 'package:eatiano_app/model/popular_dishes_model.dart';
import 'package:eatiano_app/screens/itemDetails.dart';
import 'package:flutter/material.dart';
import '../providers/popular_dishes_provider.dart';
import 'package:provider/provider.dart';

class DishViewAll extends StatefulWidget {
  static const String routeName = '/dishview-all';

  const DishViewAll({Key? key}) : super(key: key);

  @override
  DishViewAllState createState() => DishViewAllState();
}

class DishViewAllState extends State<DishViewAll> {
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      Provider.of<PopularDishesProvider>(context, listen: false).fetchData();
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    bool tabLayout = width > 600;
    bool largeLayout = width > 350 && width < 600;
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        centerTitle: true,
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        elevation: 0,
        leading: InkWell(onTap: () => Navigator.of(context).pop(), child: const Icon(Icons.arrow_back_ios, color: Colors.red)),
        title: const Text('Popular Dishes'),
      ),
      body: Selector<PopularDishesProvider, bool>(
          selector: (c, p) => p.isLoading,
          builder: (context, isLoading, child) {
            return isLoading
                ? const Center(child: CircularProgressIndicator(color: Colors.red))
                : Selector<PopularDishesProvider, List<PopularDishes>>(
                    selector: (c, p) => p.popularDishList,
                    builder: (context, data, child) {
                      return ListView.builder(
                        itemCount: data.length,
                        itemBuilder: (context, index) => InkWell(
                          onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (_) => ItemDetails(data[index].productId.toString()))),
                          child: Container(
                            margin: const EdgeInsets.all(10),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Container(
                                  height: width * 0.32,
                                  width: width * 0.32,
                                  decoration: BoxDecoration(
                                      border: Border.all(color: Colors.green, width: 2),
                                      borderRadius: BorderRadius.circular(10),
                                      boxShadow: const [BoxShadow(color: Colors.black, blurRadius: 8, offset: Offset(1, 2))]),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(10),
                                    child: Image.network(
                                      'http://13.127.95.127/eatianoBackend/public${data[index].productImage}',
                                      fit: BoxFit.cover,
                                      errorBuilder: (BuildContext context, value, stack) =>
                                          Image.asset('assets/images/icon_default_dish.png', fit: BoxFit.contain, height: height * 0.08, width: width * 0.25),
                                    ),
                                  ),
                                ),
                                SizedBox(width: width * 0.025),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      data[index].productName ?? '',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontSize: tabLayout
                                              ? 23
                                              : largeLayout
                                                  ? 18
                                                  : 13),
                                    ),
                                    SizedBox(height: height * 0.004),
                                    Text(data[index].restaurantName ?? '',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontSize: tabLayout
                                                ? 23
                                                : largeLayout
                                                    ? 18
                                                    : 13)),
                                    SizedBox(height: height * 0.004),
                                    Text('₹${data[index].productSellingPrice}',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontSize: tabLayout
                                                ? 25
                                                : largeLayout
                                                    ? 20
                                                    : 15)),
                                    SizedBox(height: height * 0.004),
                                    Row(
                                      children: [
                                        const Icon(Icons.star, color: Colors.yellow),
                                        Text(
                                          data[index].productRating ?? '4',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                              fontSize: tabLayout
                                                  ? 22
                                                  : largeLayout
                                                      ? 18
                                                      : 12),
                                        ),
                                        Text(
                                          '(${data[index].productRatingCount} Ratings)',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                              fontSize: tabLayout
                                                  ? 22
                                                  : largeLayout
                                                      ? 18
                                                      : 12),
                                        )
                                      ],
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    });
          }),
    );
  }
}
