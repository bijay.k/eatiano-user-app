import 'package:flutter/material.dart';

class Button extends StatelessWidget {
  const Button(this.text, {Key? key, required this.onPressed}) : super(key: key);
  final String text;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    bool tabLayout = width > 600;
    bool largeLayout = width > 350 && width < 600;
    return Ink(
      width: MediaQuery.of(context).size.width * 0.75,
      height: height * 0.055,
      decoration: BoxDecoration(
        color: const Color.fromRGBO(255, 0, 0, 1),
        borderRadius: BorderRadius.circular(30),
        boxShadow: const [BoxShadow(color: Colors.black, blurRadius: 20, spreadRadius: 10, offset: Offset(2, 1))],
      ),
      child: InkWell(
        onTap: onPressed,
        borderRadius: BorderRadius.circular(30),
        child: Center(
          child: Text(text,
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: tabLayout
                      ? 25
                      : largeLayout
                          ? 15
                          : 12)),
        ),
      ),
    );
  }
}
