import 'package:eatiano_app/repository/package_info_service.dart';
import 'package:eatiano_app/screens/login/widgets/signIn.dart';
import 'package:eatiano_app/utils/locator.dart';
import 'package:flutter/material.dart';
import '../../painters/logInOutBorderPaint.dart';

class LoginPage extends StatefulWidget {
  static const String routeName = '/sign_in';

  const LoginPage({Key? key}) : super(key: key);

  @override
  LoginPageState createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    var statusBarPadding = MediaQuery.of(context).padding.top;
    bool tabLayout = width > 600;
    bool largeLayout = width > 350 && width < 600;

    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(image: DecorationImage(image: AssetImage('assets/images/Hand_drawn.png'), fit: BoxFit.cover)),
        child: ListView(
          children: [
            Container(
              height: tabLayout ? (height - statusBarPadding) * 0.80 : (height - statusBarPadding) * 0.85,
              padding:
                  EdgeInsets.only(left: width * 0.04, top: (height - statusBarPadding) * 0.06, right: width * 0.04, bottom: (height - statusBarPadding) * 0.01),
              child: CustomPaint(painter: LogInOutBorderPaint(), child: const SignInForm()),
            ),
            Container(
              height: tabLayout ? (height - MediaQuery.of(context).padding.top) * 0.20 : (height - MediaQuery.of(context).padding.top) * 0.15,
              width: double.infinity,
              padding: EdgeInsets.only(top: (height - statusBarPadding) * 0.006, bottom: (height - statusBarPadding) * 0.015),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Don\'t have an Account?',
                        textScaleFactor: MediaQuery.of(context).textScaleFactor * 1.2,
                        style: TextStyle(
                            color: Colors.grey,
                            fontWeight: FontWeight.bold,
                            fontSize: tabLayout
                                ? 20
                                : largeLayout
                                    ? 15
                                    : 12),
                      ),
                      const SizedBox(width: 10),
                      InkWell(
                        onTap: () => Navigator.of(context).pushReplacementNamed('/sign-up'),
                        child: Text(
                          'Sign Up',
                          textScaleFactor: MediaQuery.of(context).textScaleFactor * 1.2,
                          style: TextStyle(
                              color: Colors.red,
                              fontWeight: FontWeight.bold,
                              fontSize: tabLayout
                                  ? 20
                                  : largeLayout
                                      ? 15
                                      : 12),
                        ),
                      )
                    ],
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: FutureBuilder<String>(
                        future: locator.get<PackageInfoService>().versionName(),
                        builder: (context, snapshot) {
                          return Text(
                            snapshot.data ?? '',
                            textScaleFactor: MediaQuery.of(context).textScaleFactor * 1.2,
                            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                          );
                        }),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
