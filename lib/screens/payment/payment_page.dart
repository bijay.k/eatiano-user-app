import 'package:eatiano_app/screens/my_orders/my_orders_page.dart';
import 'package:eatiano_app/utils/constants.dart';
import 'package:eatiano_app/utils/toast_message.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../providers/cart_provider.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';
import 'package:fluttertoast/fluttertoast.dart';
import '../../payment/payPalPayment.dart';
import '../../model/location/location.dart';
import '../../repository/profile_provider.dart';
import '../../providers/order_provider.dart';

class PaymentScreen extends StatefulWidget {
  const PaymentScreen(this.discountAmount, this.couponCode, {Key? key}) : super(key: key);
  final double discountAmount;
  final String? couponCode;

  @override
  PaymentScreenState createState() => PaymentScreenState();
}

class PaymentScreenState extends State<PaymentScreen> {
  late Razorpay razorpay;
  int _selectedValue = 1;
  bool payPalButton = true;
  bool razorPayButton = false;
  List<dynamic> orderIdList = [];
  Map<String, dynamic> ordersId = {};

  void payPal() {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (BuildContext context) => PaypalPayment(
          onFinish: (number) async {
            print('order id: ' + number);
          },
        ),
      ),
    );
  }

  // @override
  // void didChangeDependencies() {
  //   Provider.of<ProfileProvider>(context, listen: false).fetchData();
  //   Provider.of<OrderIdProvider>(context)
  //       .getOrderId('West Bengal', widget.couponCode);
  //   // TODO: implement didChangeDependencies
  //   super.didChangeDependencies();
  //   razorpay = Razorpay();
  //   razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
  //   razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
  //   razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
  // }

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
        Provider.of<ProfileProvider>(context, listen: false).fetchData();
      });
    });
    super.initState();
    razorpay = Razorpay();
    razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
  }

  Future<void> openCheckOut(BuildContext context, amount) async {
    try {
      final location = context.read<LocationProvider>();
      final details = context.read<ProfileProvider>().profileModel;
      final data = {
        'name': details?.name,
        'phone': details?.phone,
        'email': details?.email,
        'address': location.address,
        'country': details?.country,
        'pin': location.pin,
      };

      context.read<OrderProvider>().getOrderId(data, widget.couponCode ?? '').then((_) {
        final orderId = context.read<OrderProvider>().orderId;
        var options = {
          'key': Constants.razorpayKey,
          'amount': amount * 100,
          'name': 'Eatiano Order',
          "order_id": orderId?.id,
          'description': orderId?.id,
          "currency": "INR",
          'prefill': {'contact': details?.phone, 'email': details?.email}
        };

        print('options : $options');
        razorpay.open(options);
      });
      // print('PAYMENT ID AGAIN $paymentId');
      // razorpay.open(options);
    } catch (e) {
      debugPrint('Error: e');
    }
  }

  @override
  void dispose() {
    razorpay.clear();
    super.dispose();
  }

  void _handlePaymentSuccess(PaymentSuccessResponse paymentResponse) async {
    ToastMessage.showMessage('${paymentResponse.paymentId}', kToastSuccessColor);

    print('Payment Success Order ID ${paymentResponse.orderId}');
    print('Payment Success Payment ID ${paymentResponse.paymentId}');
    print('Payment Success Signature ${paymentResponse.signature}');

    final discountCalculation = context.read<CartItemProvider>().itemAmount * (widget.discountAmount / 100);
    final withDeliveryCost = (context.read<CartItemProvider>().itemAmount + context.read<CartItemProvider>().deliveryCost) - discountCalculation;
    final profile = context.read<ProfileProvider>().profileModel;
    final location = context.read<LocationProvider>();
    final orderId = context.read<OrderProvider>().orderId;
    final cartDetails = context.read<CartItemProvider>().cartList.elementAt(0);

    final data = {
      "razorpay_sig": paymentResponse.signature ?? '',
      'payment_id': paymentResponse.paymentId,
      "order_id": orderId?.id,
    };
    context.read<OrderProvider>().paymentValidation(data).then((value) {
      if(value.result['status'] == 'success') {
        final data = {
          "total_amount": withDeliveryCost.toString(),
          "marchent_name": 'razorpay',
          "transaction_amount": orderId?.amount.toString(),
          "restaurant_id": cartDetails.restaurantId,
          "transaction_id": paymentResponse.paymentId,
          "order_unique_id": orderId?.id,
          'city': 'YourCity',
          "state": 'West Bengal',
          "area": 'YourArea',
          "lat": '88',
          "lng": '45',
          "pincode": location.pin,
          "phone": profile?.phone,
          "coupon_code": 'a11',
          "signature_razorpay": paymentResponse.signature ?? '',
        };

        context.read<OrderProvider>().updateOrder(data).then((value) {
          if(value.result['status'] == 200) {
            Navigator.of(context).pushNamed(MyOrdersPage.routeName);
          }
        });
      }


    });
  }

  void _handlePaymentError(PaymentFailureResponse paymentFailureResponse) {
    Fluttertoast.showToast(msg: "ERROR: " + paymentFailureResponse.code.toString() + "-" + paymentFailureResponse.message!, toastLength: Toast.LENGTH_LONG);
    print('PAYMENT ERROR ${paymentFailureResponse.code.toString()}');
    print('PAYMENT DESCRIPTION ${paymentFailureResponse.message}');
  }

  void _handleExternalWallet(ExternalWalletResponse externalWalletResponse) {
    Fluttertoast.showToast(msg: "EXTERNAL_WALLET: " + externalWalletResponse.walletName!, toastLength: Toast.LENGTH_SHORT);
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    final textScale = MediaQuery.of(context).textScaleFactor * 1.2;
    // final route =
    //     ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>;
    // final discount = route['discount'];
    // final discountCode = route['code'];

    final discountCalculation = Provider.of<CartItemProvider>(context).itemAmount * (widget.discountAmount / 100);
    final withDeliveryCost =
        // (Provider.of<CartItemProvider>(context).itemAmount +
        //         Provider.of<CartItemProvider>(context).deliveryCost) -
        //     Provider.of<CartItemProvider>(context).discountCost;
        (Provider.of<CartItemProvider>(context).itemAmount + Provider.of<CartItemProvider>(context).deliveryCost) - discountCalculation;

    // final provider = Provider.of<ProfileProvider>(context).profile;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        elevation: 0,
        centerTitle: true,
        leading: InkWell(onTap: () => Navigator.of(context).pop(), child: const Icon(Icons.arrow_back_ios, color: Colors.red)),
        title: Text('Payment', textScaleFactor: textScale, style: const TextStyle(color: Colors.white)),
      ),
      body: ListView(
        children: [
          Container(
            margin: EdgeInsets.only(top: height * 0.02, bottom: height * 0.02),
            padding: EdgeInsets.only(left: width * 0.06, right: width * 0.06),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Delivery Address', textScaleFactor: textScale, style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15)),
                SizedBox(height: height * 0.02),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                        flex: 3,
                        child: Text(Provider.of<LocationProvider>(context).address,
                            overflow: TextOverflow.ellipsis,
                            textScaleFactor: textScale,
                            style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 12))),
                    Flexible(
                      flex: 1,
                      child: InkWell(
                        onTap: () => Navigator.of(context).pushNamed('/change-location'),
                        child: Text('Change', textScaleFactor: textScale, style: const TextStyle(color: Colors.red, fontWeight: FontWeight.bold, fontSize: 12)),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: width * 0.02, right: width * 0.02),
            child: Container(
              width: double.infinity,
              height: height * 0.001,
              margin: EdgeInsets.only(top: height * 0.01, bottom: height * 0.01),
              color: const Color.fromRGBO(161, 218, 46, 1),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: width * 0.06, right: width * 0.06),
            width: double.infinity,
            margin: EdgeInsets.only(top: height * 0.02),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Sub Total', textScaleFactor: textScale, style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 12)),
                Row(
                  children: [
                    Text('₹', textScaleFactor: textScale, style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 12)),
                    SizedBox(width: width * 0.02),
                    Text(
                        // (Provider.of<CartItemProvider>(context).totalAmount -
                        //         Provider.of<CartItemProvider>(context)
                        //             .deliveryCost)
                        //     .toString(),
                        Provider.of<CartItemProvider>(context).itemAmount.toString(),
                        textScaleFactor: textScale,
                        style: const TextStyle(
                          color: Colors.red,
                          fontWeight: FontWeight.bold,
                        )),
                  ],
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: width * 0.06, right: width * 0.06),
            width: double.infinity,
            margin: EdgeInsets.only(top: height * 0.02),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Delivery Cost', textScaleFactor: textScale, style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 12)),
                Row(
                  children: [
                    Text('₹', textScaleFactor: textScale, style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 12)),
                    SizedBox(width: width * 0.02),
                    Text(context.read<CartItemProvider>().deliveryCost.toString(),
                        textScaleFactor: textScale, style: const TextStyle(color: Colors.red, fontWeight: FontWeight.bold)),
                  ],
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: width * 0.06, right: width * 0.06),
            width: double.infinity,
            margin: EdgeInsets.only(top: height * 0.02),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Discount', textScaleFactor: textScale, style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 12)),
                Row(
                  children: [
                    Text('₹', textScaleFactor: textScale, style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 12)),
                    SizedBox(width: width * 0.02),
                    Text(widget.discountAmount.toString(), textScaleFactor: textScale, style: const TextStyle(color: Colors.red, fontWeight: FontWeight.bold)),
                  ],
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: width * 0.02, right: width * 0.02),
            child: Container(
              width: double.infinity,
              height: height * 0.002,
              margin: EdgeInsets.only(top: height * 0.01, bottom: height * 0.01),
              color: const Color.fromRGBO(161, 218, 46, 1),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: width * 0.06, right: width * 0.06),
            child: Container(
              width: double.infinity,
              margin: EdgeInsets.only(top: height * 0.02),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Total', textScaleFactor: textScale, style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 12)),
                  Row(
                    children: [
                      Text('₹', textScaleFactor: textScale, style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 12)),
                      SizedBox(width: width * 0.02),
                      Text(
                          // Provider.of<CartItemProvider>(context)
                          //     .checkOutAmount
                          //     .toString(),
                          withDeliveryCost.toString(),
                          textScaleFactor: textScale,
                          style: const TextStyle(color: Colors.red, fontWeight: FontWeight.bold)),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: width * 0.02, right: width * 0.02),
            child: Container(
              width: double.infinity,
              height: height * 0.001,
              margin: EdgeInsets.only(top: height * 0.02, bottom: height * 0.01),
              color: const Color.fromRGBO(161, 218, 46, 1),
            ),
          ),
          SizedBox(height: height * 0.01),
          Padding(
              padding: EdgeInsets.only(left: width * 0.04, right: width * 0.04),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Choose Payment Method', textScaleFactor: textScale, style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
                  SizedBox(height: height * 0.02),
                  Container(
                    width: double.infinity,
                    height: height * 0.05,
                    decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(8)),
                    child: Row(
                      children: [
                        Radio(value: 1, groupValue: _selectedValue, onChanged: (int? value) => setState(() => _selectedValue = value!)),
                        const Text('Pay with '),
                        Image.asset('assets/images/NoPath - Copy (3).png'),
                      ],
                    ),
                  ),
                  SizedBox(height: height * 0.02),
                  Container(
                    width: double.infinity,
                    height: height * 0.05,
                    decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(8)),
                    child: Row(
                      children: [
                        Radio(value: 2, groupValue: _selectedValue, onChanged: (int? value) => setState(() => _selectedValue = value!)),
                        const Text('Pay with '),
                        Image.asset('assets/images/1896px-Razorpay_logo.svg.png', width: width * 0.25),
                      ],
                    ),
                  )
                ],
              )),
          SizedBox(height: height * 0.02),
          Padding(
            padding: EdgeInsets.only(left: width * 0.08, right: width * 0.08),
            child: TextButton(
                onPressed: () => _selectedValue == 1
                    ? Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (BuildContext context) => PaypalPayment(
                            onFinish: (number) async {
                              // payment done
                              print('order id: ' + number);
                            },
                          ),
                        ),
                      )
                    : openCheckOut(
                        context,
                        // Provider.of<CartItemProvider>(context, listen: false)
                        //     .checkOutAmount
                        withDeliveryCost,
                      ),
                style: TextButton.styleFrom(
                  shape: const StadiumBorder(side: BorderSide(color: Colors.white)),
                  padding: EdgeInsets.only(left: width * 0.08, right: width * 0.08),
                  backgroundColor: Theme.of(context).scaffoldBackgroundColor,
                ),
                child: Text('CONFIRM', textScaleFactor: textScale, style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold))),
          ),
        ],
      ),
    );
  }
}
