import 'package:eatiano_app/screens/cart/cart_page.dart';
import 'package:eatiano_app/screens/itemDetails.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/cart_provider.dart';
import '../model/location/location.dart';
import '../model/popularRestaurants/popularRestaurantProvider.dart';
import '../providers/popular_dishes_provider.dart';

class SearchScreen extends StatefulWidget {
  static const String routeName = '/search-screen';

  const SearchScreen({Key? key}) : super(key: key);

  @override
  SearchScreenState createState() => SearchScreenState();
}

class SearchScreenState extends State<SearchScreen> {
  bool _toggleDropDown = false;
  final _controller = TextEditingController();
  bool switchState = true;
  bool isLoading = true;
  List<dynamic> query = [];
  List<dynamic> data = [];
  List<dynamic> queryRestaurant = [];
  List<dynamic> restauranData = [];

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      Provider.of<PopularDishesProvider>(context, listen: false).searchFoodData().then((_) {
        setState(() {
          isLoading = false;
          data = Provider.of<PopularDishesProvider>(context, listen: false).searchDish;
          query = data;
        });
      });

      Provider.of<PopularRestaurantProvider>(context, listen: false).searchRestaurant().then((_) {
        isLoading = false;
        restauranData = Provider.of<PopularRestaurantProvider>(context, listen: false).restaurantList;
        queryRestaurant = restauranData;
      });
    });
    super.initState();
  }

  searchByQuery(String search) {
    setState(() {
      query = data.where((element) => element['product_name'].toLowerCase().contains(search.toLowerCase())).toList();
    });
    setState(() {
      queryRestaurant = restauranData.where((element) => element['restaurant_name'].toLowerCase().contains(search.toLowerCase())).toList();
    });
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    bool tabLayout = width > 600;
    bool largeLayout = width > 350 && width < 600;
    final provider = Provider.of<CartItemProvider>(context).cartItems;
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 5,
          backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          titleSpacing: 0,
          toolbarHeight: 110,
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  const Text('Delivering To', style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold, fontSize: 13)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Text(
                          Provider.of<LocationProvider>(context).address,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                          style: const TextStyle(color: Colors.grey, fontWeight: FontWeight.bold, fontSize: 15),
                        ),
                      ),
                      // const SizedBox(width: 20),
                      InkWell(
                        onTap: () => Navigator.of(context).pushNamed('/change-location'),
                        child: const Icon(Icons.keyboard_arrow_down, color: Colors.red),
                      )
                    ],
                  ),
                ],
              ),
              Stack(
                children: [
                  Container(
                    height: 60,
                    width: double.infinity,
                    padding: const EdgeInsets.only(top: 8, left: 2),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Icon(Icons.search, size: 30, color: Colors.grey),
                        Expanded(
                          child: Center(
                            child: Container(
                              margin: const EdgeInsets.only(bottom: 6, right: 4),
                              padding: tabLayout
                                  ? EdgeInsets.only(left: 6)
                                  : largeLayout
                                      ? EdgeInsets.only(left: 6)
                                      : EdgeInsets.only(left: 6, right: 4, bottom: 2),
                              // height: double.infinity,
                              height: tabLayout
                                  ? 45
                                  : largeLayout
                                      ? 45
                                      : 39,
                              width: width * 0.7,
                              decoration:
                                  BoxDecoration(borderRadius: const BorderRadius.all(Radius.circular(14)), border: Border.all(color: Colors.grey, width: 2)),
                              child: Row(
                                children: [
                                  Flexible(
                                      flex: 9,
                                      fit: FlexFit.tight,
                                      child: Center(
                                        child: TextField(
                                          controller: _controller,
                                          onChanged: (value) => searchByQuery(value),
                                          autofocus: true,
                                          cursorColor: Colors.grey,
                                          style: const TextStyle(color: Colors.grey, fontSize: 18),
                                          decoration: const InputDecoration(
                                            border: InputBorder.none,
                                            // hintText:
                                            //     'Search By Restaurant or Food',
                                            // hintStyle: TextStyle(
                                            //     color: Colors.grey,
                                            //     fontSize: tabLayout
                                            //         ? 22
                                            //         : largeLayout
                                            //             ? 16
                                            //             : 12),
                                            // suffixIcon: Icon(
                                            //   Icons.send,
                                            //   color: Colors.grey,
                                            //   size: 20,
                                            // )
                                          ),
                                        ),
                                      )),
                                  Flexible(
                                    flex: 1,
                                    fit: FlexFit.tight,
                                    child: InkWell(
                                      onTap: () => Navigator.of(context).pop(),
                                      child: const Icon(Icons.close, color: Colors.grey),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        // SizedBox(width: width * 0.01),
                        Row(
                          children: [
                            InkWell(
                              onTap: () => Navigator.of(context).pushNamed(CartPage.routeName),
                              child: const Icon(Icons.shopping_cart_outlined, color: Colors.grey, size: 30),
                            ),
                            InkWell(
                              onTap: () {},
                              child: const Icon(Icons.notifications_none_outlined, color: Colors.grey, size: 30),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                  provider.isNotEmpty
                      ? Positioned(
                          right: width * 0.06,
                          top: height * 0.02,
                          child: CircleAvatar(
                            radius: 8,
                            backgroundColor: Colors.red,
                            child: Text(
                              provider.length > 9 ? '9+' : provider.length.toString(),
                              style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 10),
                            ),
                          ),
                        )
                      : Container(color: Colors.transparent),
                  Positioned(
                    right: width * 0.001,
                    top: height * 0.02,
                    child: const CircleAvatar(
                      radius: 8,
                      backgroundColor: Colors.red,
                      child: Text('9+', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 10)),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Restaurants',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: tabLayout
                          ? 18
                          : largeLayout
                              ? 16
                              : 14),
                ),
                const SizedBox(width: 10),
                Transform.scale(
                  scale: 1.2,
                  child: Switch.adaptive(
                      thumbColor: switchState ? MaterialStateProperty.all(Colors.red) : MaterialStateProperty.all(Colors.amber),
                      trackColor: switchState
                          ? MaterialStateProperty.all(const Color.fromRGBO(255, 194, 168, 1))
                          : MaterialStateProperty.all(const Color.fromRGBO(255, 250, 168, 1)),
                      value: switchState,
                      onChanged: (value) {
                        setState(() {
                          switchState = value;
                        });
                      }),
                ),
                const SizedBox(width: 10),
                Text(
                  'Food',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: tabLayout
                          ? 18
                          : largeLayout
                              ? 16
                              : 14),
                ),
              ],
            ),
            switchState
                ? (isLoading
                    ? const Center(child: CircularProgressIndicator())
                    : Expanded(
                        child: Container(
                          width: double.infinity,
                          child: ListView.builder(
                            itemBuilder: (context, index) => InkWell(
                              onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (_) => ItemDetails(query[index]['product_id'].toString()))),
                              child: Container(
                                margin: EdgeInsets.only(bottom: height * 0.01),
                                padding: EdgeInsets.only(left: width * 0.02),
                                child: Row(
                                  children: [
                                    Container(
                                      width: width * 0.25,
                                      decoration: BoxDecoration(border: Border.all(color: Colors.green, width: 2), borderRadius: BorderRadius.circular(10)),
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(10),
                                        child: Image.network(
                                          'http://13.127.95.127/eatianoBackend/public${query[index]['product_image']}',
                                          errorBuilder: (context, value, stackTrace) => Container(),
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                    SizedBox(width: width * 0.02),
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          query[index]['product_name'],
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                              fontSize: tabLayout
                                                  ? 18
                                                  : largeLayout
                                                      ? 16
                                                      : 14),
                                        ),
                                        Text(
                                          'from ${query[index]['restaurant_name']}',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                              fontSize: tabLayout
                                                  ? 18
                                                  : largeLayout
                                                      ? 16
                                                      : 14),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            ),
                            // dishProvider['data'][index]['product_name']),
                            // provider[index]['product_name'])),
                            // itemCount: dishProvider["data"].length,
                            itemCount: query.length,
                          ),
                        ),
                      ))
                : (isLoading
                    ? const Center(child: CircularProgressIndicator())
                    : Expanded(
                        child: Container(
                          width: double.infinity,
                          // color: Colors.blue,
                          child: ListView.builder(
                            itemBuilder: (context, index) => InkWell(
                              onTap: () => Navigator.of(context).pushNamed('/restaurants-screen', arguments: {
                                // 'id': _restaurants["data"][index]["id"],
                                // 'id': provider[index]['restaurant_id'],
                                'id': query[index]['restaurant_id'],
                                // 'name': _restaurants["data"][index]["name"],
                                'name': query[index]['restaurant_name'],
                                'type': query[index]["type"],
                                // 'rating': _restaurants["data"][index]["rating"],
                                'rating': query[index]['restaurant_rating'],
                                // 'image': _restaurants["data"][index]["image"],
                                'image': query[index]["restaurant_image"],
                                // 'numberOfRatings': _restaurants["data"][index]
                                //     ["numberOfRatings"]
                                'numberOfRatings': query[index]['restaurant_rating_count'],
                                'distance': query[index]['distance']
                              }),
                              child: Container(
                                margin: EdgeInsets.only(bottom: height * 0.01),
                                padding: EdgeInsets.only(left: width * 0.02),
                                child: Row(
                                  children: [
                                    Container(
                                      width: width * 0.25,
                                      // height: height * 0.2,
                                      decoration: BoxDecoration(border: Border.all(color: Colors.green, width: 2), borderRadius: BorderRadius.circular(10)),
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(10),
                                        child: Image.network(
                                          'http://13.127.95.127/eatianoBackend/public${queryRestaurant[index]['restaurant_image']}',
                                          errorBuilder: (context, value, stackTrace) => Container(),
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                    SizedBox(width: width * 0.02),
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          queryRestaurant[index]['restaurant_name'],
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                              fontSize: tabLayout
                                                  ? 18
                                                  : largeLayout
                                                      ? 16
                                                      : 14),
                                        ),
                                        SizedBox(height: height * 0.01),
                                        Text(
                                          queryRestaurant[index]['restaurant_address'],
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                              fontSize: tabLayout
                                                  ? 15
                                                  : largeLayout
                                                      ? 13
                                                      : 11),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            ),
                            itemCount: queryRestaurant.length,
                          ),
                        ),
                      ))
          ],
        ));
  }
}
