import 'package:flutter/material.dart';

class AboutUs extends StatelessWidget {
  static const String routeName = '/about-us';
  const AboutUs({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var textScale = MediaQuery.of(context).textScaleFactor * 1.2;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        centerTitle: true,
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        leading: InkWell(onTap: () => Navigator.of(context).pop(), child: const Icon(Icons.arrow_back_ios, color: Colors.red)),
        title: Text('About Us', textScaleFactor: textScale, style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );
  }
}
