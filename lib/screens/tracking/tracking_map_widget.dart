import 'dart:async';

import 'package:eatiano_app/model/order_model.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class TrackingMapWidget extends StatefulWidget {
  const TrackingMapWidget({Key? key, required this.order}) : super(key: key);
  final Order order;

  @override
  State<TrackingMapWidget> createState() => _TrackingMapWidgetState();
}

class _TrackingMapWidgetState extends State<TrackingMapWidget> {
  LatLng? _latLng;
  Completer<GoogleMapController> mapController = Completer();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      extendBody: true,
      appBar: AppBar(
        title: const Text('Track Your Order', style: TextStyle(color: Colors.black)),
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(onPressed: () => Navigator.pop(context), icon: const Icon(Icons.arrow_back_ios_new, color: Colors.orange)),
        clipBehavior: Clip.none,
        automaticallyImplyLeading: false,
      ),
      body: Stack(
        clipBehavior: Clip.none,
        children: [
          GoogleMap(
            myLocationEnabled: false,
            myLocationButtonEnabled: false,
            compassEnabled: false,
            zoomControlsEnabled: false,
            mapType: MapType.normal,
            markers: {},
            polylines: {},
            onMapCreated: (controller) async {
              mapController.complete(controller);
              // controller.googleMapController = await mapController.future;
            },
            onCameraMove: (cameraPosition) {
              // provider.con.onCameraMove!();
            },
            initialCameraPosition: const CameraPosition(target: LatLng(20.5937, 78.9629), zoom: 3.0),
            onTap: (position) {
              // provider.con.hideInfoWindow!();
            },
          ),
          Align(alignment: Alignment.bottomCenter, child: _InfoWidget(order: widget.order)),
        ],
      ),
    );
  }
}

class _InfoWidget extends StatelessWidget {
  const _InfoWidget({Key? key, required this.order}) : super(key: key);
  final Order order;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: Theme.of(context).scaffoldBackgroundColor, borderRadius: BorderRadius.circular(10)),
      margin: const EdgeInsets.all(15.0),
      padding: const EdgeInsets.all(12.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            children: [
              Expanded(
                  child: Column(
                children: [],
              )),
              Expanded(
                  child: Column(
                children: [],
              )),
            ],
          ),
          const Divider(),
          Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text(order.restaurantName ?? '', style: const TextStyle(color: Colors.white)),
                    const Text('Restaurant', style: TextStyle(color: Colors.grey)),
                  ],
                ),
              ),
              Row(
                children: [
                  IconButton(onPressed: () {}, icon: const Icon(Icons.call)),
                  IconButton(onPressed: () {}, icon: const Icon(Icons.call)),
                ],
              )
            ],
          ),
        ],
      ),
    );
  }
}
