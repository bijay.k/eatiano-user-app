import 'package:eatiano_app/model/order_model.dart';
import 'package:eatiano_app/model/track_model.dart';
import 'package:eatiano_app/providers/track_provider.dart';
import 'package:eatiano_app/screens/tracking/tracking_map_widget.dart';
import 'package:eatiano_app/widgets/progress_bar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TrackingPage extends StatefulWidget {
  static const String routeName = '/tracking';

  const TrackingPage({Key? key, required this.order}) : super(key: key);
  final Order order;

  @override
  State<TrackingPage> createState() => _TrackingPageState();
}

class _TrackingPageState extends State<TrackingPage> {
  // { status: 'Order assigned to pickup warehouse, icon: 'fa fa-check, text: 'Order Confirmed" },
  // { status: 'Pickup executive assigned', icon: 'fa fa-user', text: 'Pickup Executive Assigned'}
  // { status: 'Order picked up', icon: 'fa fa-ship', text: 'Picked By Courier'},
  // { status: 'Order shipped', icon: 'fa fa-plane', text: 'Order Shipped' },
  // { status: 'Order has reached destination warehouse', icon: 'fa fa-home, text: 'Reached Destination'
  // { status: 'Out for delivery', icon: 'fa fa-truck, text: 'Out For Delivery'),
  // { status: 'delivered', icon: 'fa fa-box', text: 'Delivered' },

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      context.read<TrackingProvider>().fetchTracking(widget.order.orderId);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        elevation: 5,
        centerTitle: true,
        leading: IconButton(onPressed: () => Navigator.of(context).pop(), icon: const Icon(Icons.arrow_back_ios_new, color: Colors.red)),
        title: const Text('Tracking Page', style: TextStyle(color: Colors.white)),
      ),
      body: Selector<TrackingProvider, bool>(
          selector: (c, p) => p.isLoading,
          builder: (c, isLoading, child) {
            return isLoading
                ? const ProgressBar()
                : Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: [
                        Container(
                          padding: const EdgeInsets.all(8.0),
                          decoration: BoxDecoration(color: const Color(0xff474D53), borderRadius: BorderRadius.circular(15)),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Text('Order No : ${widget.order.orderId}', style: const TextStyle(color: Colors.white)),
                              const Divider(),
                              Selector<TrackingProvider, TrackModel?>(
                                  selector: (c, p) => p.trackModel,
                                  builder: (context, trackModel, child) {
                                    return ListView.builder(
                                        shrinkWrap: true,
                                        itemCount: trackModel?.data?.length,
                                        itemBuilder: (context, index) {
                                          final order = trackModel?.data?.elementAt(index);
                                          return Row(
                                            children: [
                                              Expanded(
                                                child: Column(
                                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                                  children: [
                                                    Text(order?.productName ?? '', style: const TextStyle(color: Colors.white, fontWeight: FontWeight.w600)),
                                                    Text('\u{20B9} ${order?.productPrice ?? 0}', style: const TextStyle(color: Colors.white)),
                                                  ],
                                                ),
                                              ),
                                              Image.network(
                                                'http://13.127.95.127/eatianoBackend/public${order?.productImage}',
                                                errorBuilder: (c, e, s) => Image.asset('assets/images/icon_default_dish.png', height: 30, width: 30),
                                                height: 30,
                                                width: 30,
                                              ),
                                            ],
                                          );
                                        });
                                  }),
                              // Row(
                              //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              //   children: [
                              //     Text('Rating'),
                              //     Text('Rating'),
                              //   ],
                              // ),
                            ],
                          ),
                        ),
                        const SizedBox(height: 10),
                        Selector<TrackingProvider, TrackModel?>(
                            selector: (c, p) => p.trackModel,
                            builder: (context, trackModel, child) {
                              int value = 0;
                              if (trackModel != null) {
                                if (trackModel.orderStatus!.contains('Order assigned to pickup warehouse')) {
                                  value = 1;
                                } else if (trackModel.orderStatus!.contains('Pickup executive assigned')) {
                                  value = 2;
                                } else if (trackModel.orderStatus!.contains('Order picked up')) {
                                  value = 3;
                                } else if (trackModel.orderStatus!.contains('Order shipped')) {
                                  value = 4;
                                } else if (trackModel.orderStatus!.contains('Order has reached destination warehouse')) {
                                  value = 5;
                                } else if (trackModel.orderStatus!.contains('Out for delivery')) {
                                  value = 6;
                                } else if (trackModel.orderStatus!.contains('delivered')) {
                                  value = 7;
                                }
                              }
                              return Column(children: [
                                _OrderTrack(
                                    isColored: value > 0,
                                    title: 'Order assigned to pickup warehouse',
                                    image: 'assets/images/icon_order_processed.png',
                                    desc: "Order Confirmed"),
                                _OrderTrack(
                                    isColored: value > 1,
                                    title: 'Pickup executive assigned',
                                    image: 'assets/images/icon_order_pickup.png',
                                    desc: 'Pickup Executive Assigned'),
                                _OrderTrack(
                                    isColored: value > 2, title: 'Order picked up', image: 'assets/images/icon_default_dish.png', desc: 'Picked By Courier'),
                                _OrderTrack(isColored: value > 3, title: 'Order shipped', image: 'assets/images/icon_default_dish.png', desc: 'Order Shipped'),
                                _OrderTrack(
                                    isColored: value > 4,
                                    title: 'Order has reached destination warehouse',
                                    image: 'assets/images/icon_default_dish.png',
                                    desc: 'Reached Destination'),
                                _OrderTrack(
                                    isColored: value > 5, title: 'Out for delivery', image: 'assets/images/icon_default_dish.png', desc: 'Out For Delivery'),
                                _OrderTrack(isColored: value > 6, title: 'Delivered', image: 'assets/images/icon_delivery.png', desc: 'Delivered'),
                              ]);
                            }),
                        ElevatedButton(
                            onPressed: () {
                              Navigator.push(context, MaterialPageRoute(builder: (context) => TrackingMapWidget(order: widget.order)));
                            },
                            child: const Text('Tracking map'))
                      ],
                    ),
                  );
          }),
    );
  }
}

class _OrderTrack extends StatelessWidget {
  const _OrderTrack({Key? key, required this.isColored, required this.title, required this.desc, this.time, required this.image}) : super(key: key);
  final String image, title, desc;
  final String? time;
  final bool isColored;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Container(
              padding: const EdgeInsets.all(5),
              decoration: BoxDecoration(color: isColored ? Colors.green : const Color(0xff474D53), shape: BoxShape.circle),
              child: CircleAvatar(radius: 30, backgroundColor: isColored ? Colors.green : const Color(0xff474D53), foregroundImage: AssetImage(image)),
            ),
            const SizedBox(width: 10),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(title, style: TextStyle(color: isColored ? Colors.white : const Color(0xff474D53))),
                Text(desc, style: TextStyle(color: isColored ? Colors.white : const Color(0xff474D53))),
                Text(time ?? '', style: TextStyle(color: isColored ? Colors.white : const Color(0xff474D53))),
              ],
            ),
          ],
        ),
        const SizedBox(height: 10),
      ],
    );
  }
}
