import 'package:eatiano_app/model/cart_model.dart';
import 'package:eatiano_app/screens/cart/cart_page.dart';
import 'package:eatiano_app/screens/changeLocation.dart';
import 'package:eatiano_app/screens/searchScreen.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../providers/cart_provider.dart';
import '../../model/location/location.dart';
import '../../notificationService/localNotificationService.dart';
import '../../widgets/home/homeContent.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    FirebaseMessaging.instance.getInitialMessage().then(
      (message) {
        if (message != null) {
          print("New Notification");
          // if (message.data['_id'] != null) {
          //   Navigator.of(context).push(
          //     MaterialPageRoute(
          //       builder: (context) => DemoScreen(
          //         id: message.data['_id'],
          //       ),
          //     ),
          //   );
          // }
        }
      },
    );

    // 2. This method only call when App in forground it mean app must be opened
    FirebaseMessaging.onMessage.listen(
      (message) {
        print("FirebaseMessaging.onMessage.listen");
        if (message.notification != null) {
          print(message.notification!.title);
          print(message.notification!.body);
          print("message.data11 ${message.data}");
          LocalNotificationService.createanddisplaynotification(message);
        }
      },
    );

    // 3. This method only call when App in background and not terminated(not closed)
    FirebaseMessaging.onMessageOpenedApp.listen(
      (message) {
        print("FirebaseMessaging.onMessageOpenedApp.listen");
        if (message.notification != null) {
          print(message.notification!.title);
          print(message.notification!.body);
          print("message.data22 ${message.data['_id']}");
        }
      },
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    bool tabLayout = width > 600;
    bool largeLayout = width > 350 && width < 600;

    return Scaffold(
      appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 5,
          backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          titleSpacing: 0,
          toolbarHeight: tabLayout ? height * 0.15 : height * 0.13,
          title: Column(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Delivering To',
                      style: TextStyle(
                          color: Colors.grey,
                          fontWeight: FontWeight.bold,
                          fontSize: tabLayout
                              ? 18
                              : largeLayout
                                  ? 13
                                  : 10),
                    ),
                    const SizedBox(height: 5),
                    Row(
                      children: [
                        Expanded(
                          child: Text(
                            Provider.of<LocationProvider>(context).address,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            style: TextStyle(
                                color: Colors.grey,
                                fontWeight: FontWeight.bold,
                                fontSize: tabLayout
                                    ? 20
                                    : largeLayout
                                        ? 12
                                        : 10),
                          ),
                        ),
                        const SizedBox(width: 10),
                        InkWell(
                          onTap: () => Navigator.of(context).pushNamed(ChangeLocation.routeName),
                          child: Icon(Icons.keyboard_arrow_down, color: Colors.red, size: tabLayout ? 30 : 20),
                        )
                      ],
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 5),
              Stack(
                children: [
                  Container(
                    height: height * 0.055,
                    width: double.infinity,
                    padding: const EdgeInsets.only(left: 6),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Icon(
                          Icons.search,
                          size: tabLayout
                              ? 45
                              : largeLayout
                                  ? 30
                                  : 20,
                          color: Colors.grey,
                        ),
                        // if (_searchIcon)
                        Expanded(
                          child: Center(
                            child: InkWell(
                              onTap: () => Navigator.of(context).pushNamed(SearchScreen.routeName),
                              child: Container(
                                margin: EdgeInsets.only(
                                    bottom: tabLayout
                                        ? 2
                                        : largeLayout
                                            ? 6
                                            : 0,
                                    right: 4),
                                padding: EdgeInsets.only(left: tabLayout ? 15 : 6),
                                height: tabLayout
                                    ? height * 0.06
                                    : largeLayout
                                        ? 45
                                        : 60,
                                width: tabLayout ? width * 0.82 : width * 0.7,
                                decoration:
                                    BoxDecoration(borderRadius: const BorderRadius.all(Radius.circular(14)), border: Border.all(color: Colors.grey, width: 2)),
                                child: Row(
                                  children: [
                                    Text(
                                      'Search By Restaurant or Food',
                                      style: TextStyle(
                                        color: Colors.grey,
                                        fontSize: tabLayout
                                            ? 22
                                            : largeLayout
                                                ? 16
                                                : 12,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(width: 10),
                        Row(
                          children: [
                            InkWell(
                              onTap: () => Navigator.of(context).pushNamed(CartPage.routeName),
                              child: Icon(
                                Icons.shopping_cart_outlined,
                                color: Colors.grey,
                                size: tabLayout
                                    ? 38
                                    : largeLayout
                                        ? 30
                                        : 20,
                              ),
                            ),
                            InkWell(
                              onTap: () {},
                              child: Icon(
                                Icons.notifications_none_outlined,
                                color: Colors.grey,
                                size: tabLayout
                                    ? 38
                                    : largeLayout
                                        ? 30
                                        : 20,
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                  Selector<CartItemProvider, List<CartModel>>(
                      selector: (c, p) => p.cartList,
                      builder: (context, cartItems, child) {
                        return cartItems.isNotEmpty
                            ? Positioned(
                                right: tabLayout ? width * 0.035 : width * 0.06,
                                top: tabLayout
                                    ? height * 0.01
                                    : largeLayout
                                        ? height * 0.02
                                        : height * 0.006,
                                child: CircleAvatar(
                                  radius: tabLayout
                                      ? 10
                                      : largeLayout
                                          ? 8
                                          : 6,
                                  backgroundColor: Colors.red,
                                  child: Text(
                                    cartItems.length > 9 ? '9+' : cartItems.length.toString(),
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontSize: tabLayout
                                            ? 15
                                            : largeLayout
                                                ? 10
                                                : 8),
                                  ),
                                ),
                              )
                            : Container(color: Colors.transparent);
                      }),
                  Positioned(
                    right: width * 0.001,
                    top: tabLayout
                        ? height * 0.01
                        : largeLayout
                            ? height * 0.02
                            : height * 0.006,
                    child: CircleAvatar(
                      radius: tabLayout
                          ? 10
                          : largeLayout
                              ? 8
                              : 6,
                      backgroundColor: Colors.red,
                      child: Text(
                        '9+',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: tabLayout
                                ? 15
                                : largeLayout
                                    ? 10
                                    : 8),
                      ),
                    ),
                  )
                ],
              )
            ],
          )),
      body: const HomeContent(),
    );
  }
}
