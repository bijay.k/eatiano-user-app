import 'package:eatiano_app/model/coupon/coupo_model.dart';
import 'package:eatiano_app/widgets/progress_bar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../model/coupon/couponProvider.dart';
import '../../providers/cart_provider.dart';

class CouponPage extends StatefulWidget {
  static const String routeName = '/coupon-screen';

  const CouponPage({Key? key}) : super(key: key);

  @override
  CouponPageState createState() => CouponPageState();
}

class CouponPageState extends State<CouponPage> {
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      context.read<CouponProvider>().fetchCoupons();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    final price = Provider.of<CartItemProvider>(context).itemAmount;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        elevation: 0,
        automaticallyImplyLeading: false,
        centerTitle: true,
        leading: InkWell(onTap: () => Navigator.of(context).pop(), child: const Icon(Icons.arrow_back_ios, color: Colors.red)),
        title: const Text('Available Offers'),
      ),
      body: Selector<CouponProvider, bool>(
          selector: (c, p) => p.isLoading,
          builder: (context, isLoading, child) {
            return isLoading
                ? const ProgressBar()
                : Padding(
                    padding: const EdgeInsets.all(10),
                    child: Selector<CouponProvider, List<CouponData>>(
                        selector: (c, p) => p.couponList,
                        builder: (context, provider, child) {
                          return ListView.builder(
                            itemCount: provider.length,
                            itemBuilder: (context, index) => price >= double.parse(provider[index].condition)
                                ? InkWell(
                                    onTap: () async {
                                      context.read<CouponProvider>().selectedCoupon(provider[index].couponCode, double.parse(provider[index].discount));
                                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                          backgroundColor: Colors.white,
                                          content: Text(
                                            'Coupon ${provider[index].couponCode} Applied',
                                            style: const TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
                                          )));
                                      Navigator.of(context).pop();
                                      // Navigator.of(context)
                                      //     .pushNamed('/cart-screen-detail');
                                    },
                                    child: Container(
                                      margin: EdgeInsets.only(bottom: height * 0.025),
                                      padding: EdgeInsets.only(left: width * 0.04, top: height * 0.005),
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(20),
                                          color: Colors.white,
                                          boxShadow: const [BoxShadow(color: Colors.black, offset: Offset(0, 2), blurRadius: 8)]),
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            provider[index].couponCode,
                                            style: const TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
                                          ),
                                          Text.rich(
                                            TextSpan(
                                              text: 'Applicable on Orders of ',
                                              children: [
                                                TextSpan(
                                                    text: '₹${provider[index].condition}',
                                                    style: const TextStyle(color: Colors.red, fontWeight: FontWeight.bold)),
                                                const TextSpan(text: ' or above'),
                                              ],
                                            ),
                                            style: const TextStyle(color: Colors.grey),
                                          ),
                                          Text.rich(TextSpan(children: [
                                            const TextSpan(text: 'Discount Value ', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
                                            TextSpan(
                                              text: '₹${provider[index].discount}',
                                              style: const TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
                                            ),
                                          ])),
                                        ],
                                      ),
                                    ),
                                  )
                                : Container(
                                    margin: EdgeInsets.only(bottom: height * 0.025),
                                    padding: EdgeInsets.only(left: width * 0.04, top: height * 0.005),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(20),
                                        color: const Color.fromRGBO(210, 210, 206, 1),
                                        boxShadow: const [BoxShadow(color: Colors.white, offset: Offset(0, 2), blurRadius: 5)]),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(provider[index].couponCode, style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
                                        Text.rich(
                                          TextSpan(
                                            text: 'Applicable on Orders of ',
                                            children: [
                                              TextSpan(
                                                  text: '₹${provider[index].condition}',
                                                  style: const TextStyle(color: Colors.red, fontWeight: FontWeight.bold)),
                                              const TextSpan(text: ' or above'),
                                            ],
                                          ),
                                          style: const TextStyle(color: Colors.grey),
                                        ),
                                        Text.rich(TextSpan(children: [
                                          const TextSpan(text: 'Discount Value ', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
                                          TextSpan(
                                            text: '₹${provider[index].discount}',
                                            style: const TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
                                          ),
                                        ])),
                                      ],
                                    ),
                                  ),
                          );
                        }),
                  );
          }),
    );
  }
}
