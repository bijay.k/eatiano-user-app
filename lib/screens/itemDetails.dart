import 'package:eatiano_app/model/itemDetails/itemDetailsModel.dart';
import 'package:eatiano_app/widgets/itemList/moreItems.dart';
import 'package:eatiano_app/widgets/progress_bar.dart';
import 'package:flutter/material.dart';
import '../utils/curvedAppBar.dart';
import 'package:provider/provider.dart';
import '../providers/cart_provider.dart';
import '../model/itemDetails/itemDetailsProvider.dart';

class ItemDetails extends StatefulWidget {
  @override
  ItemDetailsState createState() => ItemDetailsState();
  final String productId;

  const ItemDetails(this.productId, {Key? key}) : super(key: key);
}

class ItemDetailsState extends State<ItemDetails> {
  int quantity = 1;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      Provider.of<ItemDetailsProvider>(context, listen: false).getItemDetails(widget.productId);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    bool tabLayout = width > 600;
    bool largeLayout = width > 350 && width < 600;

    return Selector<ItemDetailsProvider, bool>(
        selector: (c, p) => p.isLoading,
        builder: (context, isLoading, child) {
          return isLoading
              ? const Center(child: CircularProgressIndicator(color: Colors.red))
              : Scaffold(
                  body: Selector<ItemDetailsProvider, ItemDetail?>(
                      selector: (c, p) => p.itemDetail,
                      builder: (context, provider, child) {
                        return !tabLayout && !largeLayout
                            ? ListView(
                                children: [
                                  Stack(
                                    children: [
                                      SizedBox(height: height * 0.38, width: double.infinity),
                                      ClipPath(
                                        clipper: CurvedAppBar(),
                                        child: Container(
                                            height: height * 0.35,
                                            width: double.infinity,
                                            decoration: const BoxDecoration(
                                                color: Color.fromRGBO(58, 69, 84, 1),
                                                boxShadow: [BoxShadow(color: Colors.black, spreadRadius: 5, blurRadius: 5, offset: Offset(0, 5))])),
                                      ),
                                      Positioned(
                                          top: height * 0.16,
                                          left: width * 0.3,
                                          child: Image.network(
                                            'http://13.127.95.127/eatianoBackend/public${provider?.productImage}',
                                            height: height * 0.2,
                                            errorBuilder: (BuildContext context, value, stack) => Image.asset('assets/images/icon_default_dish.png',
                                                fit: BoxFit.contain, height: height * 0.08, width: width * 0.25),
                                          )),
                                      Positioned(
                                        top: height * 0.02,
                                        child: SizedBox(
                                          width: width * 0.999,
                                          height: height * 0.1,
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: [
                                              InkWell(
                                                onTap: () => Navigator.of(context).pop(),
                                                child: Container(
                                                  decoration:
                                                      const BoxDecoration(boxShadow: [BoxShadow(color: Colors.black, blurRadius: 10, offset: Offset(0, 0))]),
                                                  child: const CircleAvatar(
                                                    radius: 12,
                                                    backgroundColor: Colors.white,
                                                    child: Icon(Icons.keyboard_arrow_left, color: Colors.red),
                                                  ),
                                                ),
                                              ),
                                              SizedBox(width: width * 0.05),
                                              Text(provider?.productName ?? '',
                                                  style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15))
                                            ],
                                          ),
                                        ),
                                      ),
                                      Positioned(
                                        top: height * 0.1,
                                        left: width * 0.45,
                                        child: Text(
                                          '₹ ${provider?.productSellingPrice}',
                                          style: const TextStyle(color: Colors.white, fontSize: 16),
                                        ),
                                      )
                                    ],
                                  ),
                                  SizedBox(height: height * 0.02),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      InkWell(
                                        onTap: () => setState(() => quantity++),
                                        child: const CircleAvatar(
                                          radius: 10,
                                          backgroundColor: Color.fromRGBO(58, 69, 84, 1),
                                          child: Icon(Icons.add, color: Colors.white, size: 15),
                                        ),
                                      ),
                                      SizedBox(width: width * 0.05),
                                      Text(
                                        quantity.toString(),
                                        style: const TextStyle(color: Colors.white, fontSize: 25, fontWeight: FontWeight.bold),
                                      ),
                                      SizedBox(width: width * 0.05),
                                      InkWell(
                                        onTap: () => setState(() => quantity > 0 ? quantity-- : quantity = 0),
                                        child: const CircleAvatar(
                                          radius: 10,
                                          backgroundColor: Color.fromRGBO(58, 69, 84, 1),
                                          child: Icon(Icons.remove, color: Colors.white, size: 13),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: height * 0.05),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Padding(
                                          padding: EdgeInsets.only(left: width * 0.05, right: width * 0.05),
                                          child: Text(
                                            provider?.productDescription ?? '',
                                            // textScaleFactor: textScale,
                                            textAlign: TextAlign.center,
                                            style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 12),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: height * 0.04),
                                  quantity > 0
                                      ? InkWell(
                                          onTap: () {
                                            context.read<CartItemProvider>().addItems(provider!.productId!, quantity, provider.restaurantId!.toString());
                                          },
                                          child: Container(
                                            width: width * 0.5,
                                            height: height * 0.075,
                                            margin: EdgeInsets.only(left: width * 0.2, right: width * 0.2),
                                            decoration: BoxDecoration(
                                                // color: const Color.fromRGBO(168, 236, 38, 1),
                                                color: const Color.fromRGBO(58, 69, 84, 1),
                                                borderRadius: BorderRadius.circular(30),
                                                boxShadow: const [BoxShadow(color: Colors.grey, blurRadius: 5, offset: Offset(1, 2))]),
                                            child: const Center(
                                              child: Text('Add To Cart', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15)),
                                            ),
                                          ),
                                        )
                                      : Container(
                                          width: width * 0.5,
                                          height: height * 0.075,
                                          decoration: BoxDecoration(color: const Color.fromRGBO(58, 69, 84, 1), borderRadius: BorderRadius.circular(30)),
                                          child: const Center(
                                            child: Text('Add To Cart', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15)),
                                          ),
                                        ),
                                  SizedBox(height: height * 0.05),
                                  Padding(
                                    padding: EdgeInsets.only(left: width * 0.05),
                                    child: const Row(
                                      children: [
                                        Expanded(
                                          child: Text('Get more interested with this',
                                              style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 13)),
                                        )
                                      ],
                                    ),
                                  ),
                                  // Expanded(child: MoreItems())
                                  SizedBox(width: double.infinity, height: height * 0.3, child: const MoreItems())
                                ],
                              )
                            : Column(
                                children: [
                                  Stack(
                                    children: [
                                      SizedBox(height: tabLayout || largeLayout ? height * 0.32 : height * 0.38, width: double.infinity),
                                      ClipPath(
                                        clipper: CurvedAppBar(),
                                        child: Container(
                                            height: tabLayout
                                                ? height * 0.28
                                                : largeLayout
                                                    ? height * 0.28
                                                    : height * 0.35,
                                            width: double.infinity,
                                            decoration: const BoxDecoration(
                                                color: Color.fromRGBO(58, 69, 84, 1),
                                                boxShadow: [BoxShadow(color: Colors.black, spreadRadius: 5, blurRadius: 5, offset: Offset(0, 5))])),
                                      ),
                                      Positioned(
                                          top: tabLayout
                                              ? height * 0.14
                                              : largeLayout
                                                  ? height * 0.08
                                                  : height * 0.16,
                                          left: tabLayout
                                              ? height * 0.14
                                              : largeLayout
                                                  ? width * 0.08
                                                  : width * 0.3,
                                          child: Image.network(
                                            'http://13.127.95.127/eatianoBackend/public${provider?.productImage}',
                                            height: tabLayout
                                                ? height * 0.4
                                                : largeLayout
                                                    ? height * 0.3
                                                    : height * 0.2,
                                            errorBuilder: (BuildContext context, value, stack) => Image.asset('assets/images/icon_default_dish.png',
                                                fit: BoxFit.contain, height: height * 0.08, width: width * 0.25),
                                          )),
                                      Positioned(
                                        top: height * 0.02,
                                        child: SizedBox(
                                          width: tabLayout ? double.infinity : width * 0.999,
                                          height: height * 0.1,
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: [
                                              InkWell(
                                                onTap: () => Navigator.of(context).pop(),
                                                child: Container(
                                                  decoration:
                                                      const BoxDecoration(boxShadow: [BoxShadow(color: Colors.black, blurRadius: 10, offset: Offset(0, 0))]),
                                                  child: CircleAvatar(
                                                    radius: tabLayout
                                                        ? 30
                                                        : largeLayout
                                                            ? 15
                                                            : 12,
                                                    backgroundColor: Colors.white,
                                                    child: const Icon(Icons.keyboard_arrow_left, color: Colors.red),
                                                  ),
                                                ),
                                              ),
                                              SizedBox(width: width * 0.05),
                                              Text(provider?.productName ?? '',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontWeight: FontWeight.bold,
                                                      fontSize: tabLayout
                                                          ? 30
                                                          : largeLayout
                                                              ? 22
                                                              : 15))
                                            ],
                                          ),
                                        ),
                                      ),
                                      Positioned(
                                        top: height * 0.1,
                                        left: width * 0.45,
                                        child: Text(
                                          '₹ ${provider?.productSellingPrice}',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: tabLayout
                                                  ? 25
                                                  : largeLayout
                                                      ? 18
                                                      : 16),
                                        ),
                                      )
                                    ],
                                  ),
                                  SizedBox(height: height * 0.02),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      InkWell(
                                        onTap: () => setState(() => quantity++),
                                        child: const CircleAvatar(
                                          radius: 10,
                                          backgroundColor: Color.fromRGBO(58, 69, 84, 1),
                                          child: Icon(Icons.add, color: Colors.white, size: 15),
                                        ),
                                      ),
                                      SizedBox(width: width * 0.05),
                                      Text(
                                        quantity.toString(),
                                        style: const TextStyle(color: Colors.white, fontSize: 25, fontWeight: FontWeight.bold),
                                      ),
                                      SizedBox(width: width * 0.05),
                                      InkWell(
                                        onTap: () => setState(() => quantity > 0 ? quantity-- : quantity = 0),
                                        child: const CircleAvatar(
                                          radius: 10,
                                          backgroundColor: Color.fromRGBO(58, 69, 84, 1),
                                          child: Icon(Icons.remove, color: Colors.white, size: 13),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: height * 0.05),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Padding(
                                          padding: EdgeInsets.only(left: width * 0.05, right: width * 0.05),
                                          child: Text(
                                            provider?.productDescription ?? '',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                                fontSize: tabLayout
                                                    ? 20
                                                    : largeLayout
                                                        ? 15
                                                        : 12),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: height * 0.04),
                                  Selector<CartItemProvider, bool>(
                                    selector: (c, p) => p.isLoading,
                                    builder: (context, isLoading, child) =>
                                        isLoading ? Column(children: [const ProgressBar(), SizedBox(height: height * 0.04)]) : Container(),
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    width: width * 0.5,
                                    height: tabLayout || largeLayout ? height * 0.06 : height * 0.075,
                                    margin: EdgeInsets.only(left: width * 0.2, right: width * 0.2),
                                    decoration: BoxDecoration(
                                        color: quantity > 0 ? const Color.fromRGBO(168, 236, 38, 1) : const Color.fromRGBO(58, 69, 84, 1),
                                        borderRadius: BorderRadius.circular(30),
                                        boxShadow: quantity > 0 ? const [BoxShadow(color: Colors.grey, blurRadius: 5, offset: Offset(1, 2))] : null),
                                    child: InkWell(
                                      borderRadius: BorderRadius.circular(30),
                                      onTap: () {
                                        context.read<CartItemProvider>().addItems(provider!.productId!, quantity, provider.restaurantId!.toString());
                                      },
                                      child: Text(
                                        'Add To Cart',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: quantity > 0 ? null : Colors.white,
                                            fontSize: tabLayout
                                                ? 22
                                                : largeLayout
                                                    ? 18
                                                    : 15),
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: height * 0.05),
                                  Padding(
                                    padding: EdgeInsets.only(left: width * 0.05),
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: Text(
                                            'Get more interested with this',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                                fontSize: tabLayout
                                                    ? 20
                                                    : largeLayout
                                                        ? 16
                                                        : 13),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  SizedBox(height: height * 0.25, child: const MoreItems()),
                                ],
                              );
                      }));
        });
  }
}
