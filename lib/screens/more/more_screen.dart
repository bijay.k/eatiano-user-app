import 'package:eatiano_app/screens/about_us/about_us.dart';
import 'package:eatiano_app/screens/cart/cart_page.dart';
import 'package:eatiano_app/screens/my_orders/my_orders_page.dart';
import 'package:flutter/material.dart';
import '../../widgets/more/notifyBell.dart';
import '../../widgets/more/listItem.dart';

class MoreScreen extends StatelessWidget {
  static const String routeName = '/more';
  const MoreScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final textScale = MediaQuery.of(context).textScaleFactor * 1.2;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        automaticallyImplyLeading: false,
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        elevation: 0,
        title: Text('More', textScaleFactor: textScale, style: const TextStyle(color: Colors.white)),
        actions: [
          InkWell(
            onTap: () => Navigator.of(context).pushNamed(CartPage.routeName),
            child: const Icon(Icons.shopping_cart_outlined, color: Colors.grey),
          ),
          NotifyBell(),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(children: [
          const ListItem('assets/images/Path 10146.png', 'Wishlist', '/wishlist-screen'),
          SizedBox(height: height * 0.02),
          // ListItem('assets/images/Path 10146.png', 'Payment Details', ''),
          // SizedBox(height: height * 0.02),
          const ListItem('assets/images/Group 8094.png', 'My Orders', MyOrdersPage.routeName),
          SizedBox(height: height * 0.02),
          const ListItem('assets/images/003-bell.png', 'Notifications', '/notification-screen'),
          // SizedBox(height: height * 0.02),
          // ListItem('assets/images/Icon material-rate-review.png', 'Inbox',
          //     '/inbox-screen'),
          // SizedBox(height: height * 0.02),
          // ListItem('assets/images/Icon awesome-user-friends.png',
          //     'Refer A Friend', '/refer-screen'),
          SizedBox(height: height * 0.02),
          const ListItem('assets/images/Icon material-rate-review.png', 'Reviews', '/reviews-screen'),
          SizedBox(height: height * 0.02),
          const ListItem('assets/images/restaurant-membership-card-tool copy.png', 'Membership', '/membership-screen'),
          SizedBox(height: height * 0.02),
          const ListItem('assets/images/Path 14675.png', 'About Us', AboutUs.routeName),
          SizedBox(height: height * 0.05),
        ]),
      ),
    );
  }
}
