import 'package:eatiano_app/model/popular_dishes_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../providers/popular_dishes_provider.dart';

class WishlistPage extends StatefulWidget {
  static const String routeName = '/wishlist-screen';

  const WishlistPage({Key? key}) : super(key: key);

  @override
  WishlistPageState createState() => WishlistPageState();
}

class WishlistPageState extends State<WishlistPage> {
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      context.read<PopularDishesProvider>().fetchFavouriteData();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    final textScale = MediaQuery.of(context).textScaleFactor * 1.2;

    return Scaffold(
      appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          elevation: 0,
          centerTitle: true,
          leading: InkWell(onTap: () => Navigator.of(context).pop(), child: const Icon(Icons.arrow_back_ios, color: Colors.red)),
          title: const Text('Wishlist', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold))),
      body: Selector<PopularDishesProvider, bool>(
          selector: (c, p) => p.isLoading,
          builder: (context, isLoading, child) {
            return isLoading
                ? const Center(child: CircularProgressIndicator(color: Colors.red))
                : Selector<PopularDishesProvider, List<PopularDishes>>(
                    selector: (c, p) => p.popularDishList,
                    builder: (context, popularDishList, child) {
                      return ListView.builder(
                        itemCount: popularDishList.length,
                        itemBuilder: (context, index) {
                          final dish = popularDishList.elementAt(index);
                          return Padding(
                            padding: EdgeInsets.only(left: width * 0.04, right: width * 0.04),
                            child: Container(
                                height: height * 0.12,
                                width: double.infinity,
                                margin: EdgeInsets.only(bottom: height * 0.02),
                                child: Row(
                                  children: [
                                    Container(
                                        height: height * 0.12,
                                        width: width * 0.3,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(20),
                                            border: Border.all(color: const Color.fromRGBO(192, 232, 14, 1), width: 2, style: BorderStyle.solid)),
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.circular(20),
                                          child: Image.network('http://13.127.95.127/eatianoBackend/public${dish.productImage}',
                                              errorBuilder: (context, value, stackTrace) => Container(),
                                              fit: BoxFit.cover),
                                        )),
                                    Expanded(
                                      child: Container(
                                        height: height * 0.4,
                                        padding: EdgeInsets.only(left: width * 0.02),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: [
                                                Text(dish.productName ?? '',
                                                    textScaleFactor: textScale, style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
                                                SizedBox(height: height * 0.004),
                                                Text(dish.restaurantName ?? '',
                                                    textScaleFactor: textScale, style: const TextStyle(color: Colors.grey, fontWeight: FontWeight.bold)),
                                                SizedBox(height: height * 0.01),
                                                Row(
                                                  children: [
                                                    Text('₹',
                                                        textScaleFactor: textScale, style: const TextStyle(color: Colors.red, fontWeight: FontWeight.bold)),
                                                    Text(dish.productSellingPrice ?? '',
                                                        textScaleFactor: textScale, style: const TextStyle(color: Colors.red, fontWeight: FontWeight.bold))
                                                  ],
                                                )
                                              ],
                                            ),
                                            Center(
                                              child: IconButton(
                                                onPressed: () => context.read<PopularDishesProvider>().deleteFavouriteData(dish.productId.toString()),
                                                icon: Icon(Icons.delete, color: Colors.red, size: height * 0.035),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                )),
                          );
                        },
                      );
                    });
          }),
    );
  }

// void deleteItem(String id) async {
//   final response =
//       await Provider.of<PopularDishesProvider>(context, listen: false)
//           .deleteFavouriteData(id);
//   var body = json.decode(response.body);
//   if (body['status'] == 'success') {
//     ScaffoldMessenger.of(context).showSnackBar(SnackBar(
//       content: const Text('Item Removed from Wishlist',
//           style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
//       backgroundColor: Colors.white,
//       action: SnackBarAction(
//           label: 'Close',
//           onPressed: () => Scaffold.of(context).hideCurrentSnackBar()),
//     ));
//   }
// }
}
