import 'package:eatiano_app/repository/profile_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EditProfile extends StatefulWidget {
  const EditProfile({Key? key}) : super(key: key);

  @override
  EditProfileState createState() => EditProfileState();
}

class EditProfileState extends State<EditProfile> {
  final key = GlobalKey<FormState>();
  final _emailFocus = FocusNode();
  final _mobileNoFocus = FocusNode();
  final _addressFocus = FocusNode();
  final _pincodeFocus = FocusNode();
  final _passwordFocus = FocusNode();
  final _confirmPasswordFocus = FocusNode();

  @override
  void dispose() {
    _emailFocus.dispose();
    _mobileNoFocus.dispose();
    _addressFocus.dispose();
    _pincodeFocus.dispose();
    _passwordFocus.dispose();
    _confirmPasswordFocus.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    final textScale = MediaQuery.of(context).textScaleFactor * 1.2;

    return Consumer<ProfileProvider>(builder: (context, provider, child) {
      return Padding(
        padding: EdgeInsets.only(left: width * 0.1, right: width * 0.1),
        child: Center(
          child: Form(
            key: key,
            child: Column(
              children: [
                Container(
                    width: double.infinity,
                    height: height * 0.06,
                    padding: EdgeInsets.only(left: width * 0.05),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                      color: Colors.white,
                    ),
                    child: TextFormField(
                      autofocus: true,
                      keyboardType: TextInputType.name,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: provider.profileModel?.name ?? '',
                          label: Text('Name', textScaleFactor: textScale, style: const TextStyle(color: Colors.grey, fontSize: 12))),
                    )),
                SizedBox(height: height * 0.01),
                Container(
                    width: double.infinity,
                    height: height * 0.06,
                    padding: EdgeInsets.only(left: width * 0.05),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                      color: Colors.white,
                    ),
                    child: TextFormField(
                        autofocus: true,
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Email',
                            label: Text('Email', textScaleFactor: textScale, style: const TextStyle(color: Colors.grey, fontSize: 12))))),
                SizedBox(height: height * 0.01),
                Container(
                    width: double.infinity,
                    height: height * 0.06,
                    padding: EdgeInsets.only(left: width * 0.05),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                      color: Colors.white,
                    ),
                    child: TextFormField(
                        // autofocus: true,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            label: Text(
                              'Mobile No',
                              textScaleFactor: textScale,
                              style: const TextStyle(color: Colors.grey, fontSize: 12),
                            )))),
                SizedBox(height: height * 0.01),
                Container(
                    width: double.infinity,
                    height: height * 0.06,
                    padding: EdgeInsets.only(left: width * 0.05),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                      color: Colors.white,
                    ),
                    child: TextFormField(
                        // autofocus: true,
                        keyboardType: TextInputType.streetAddress,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            label: Text(
                              'Address',
                              textScaleFactor: textScale,
                              style: const TextStyle(color: Colors.grey, fontSize: 12),
                            )))),
                SizedBox(height: height * 0.01),
                Container(
                    width: double.infinity,
                    height: height * 0.06,
                    padding: EdgeInsets.only(left: width * 0.05),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                      color: Colors.white,
                    ),
                    child: TextFormField(
                        // autofocus: true,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            label: Text(
                              'Pincode',
                              textScaleFactor: textScale,
                              style: const TextStyle(color: Colors.grey, fontSize: 12),
                            )))),
                SizedBox(height: height * 0.01),
                Container(
                    width: double.infinity,
                    height: height * 0.06,
                    padding: EdgeInsets.only(left: width * 0.05),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                      color: Colors.white,
                    ),
                    child: TextFormField(
                        // autofocus: true,
                        // keyboardType: TextInputType.,
                        obscureText: true,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            label: Text(
                              'Password',
                              textScaleFactor: textScale,
                              style: const TextStyle(color: Colors.grey, fontSize: 12),
                            )))),
                SizedBox(height: height * 0.01),
                Container(
                    width: double.infinity,
                    height: height * 0.06,
                    padding: EdgeInsets.only(left: width * 0.05),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                      color: Colors.white,
                    ),
                    child: TextFormField(
                        // autofocus: true,
                        // keyboardType: TextInputType.,
                        obscureText: true,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            label: Text(
                              'Confirm Password',
                              textScaleFactor: textScale,
                              style: const TextStyle(color: Colors.grey, fontSize: 12),
                            )))),
                SizedBox(height: height * 0.05),
                InkWell(
                  borderRadius: BorderRadius.circular(100),
                  onTap: () {
                    context.read<ProfileProvider>().editProfile();
                    context.read<ProfileProvider>().setEdit();
                  },
                  child: Container(
                    width: double.infinity,
                    height: height * 0.05,
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(100), border: Border.all(color: Colors.white, width: 1)),
                    child: Center(
                      child: Text(
                        'Save',
                        textScaleFactor: textScale,
                        style: const TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    });
  }
}
