import 'package:eatiano_app/repository/profile_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProfileDetails extends StatelessWidget {
  const ProfileDetails({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;

    return Consumer<ProfileProvider>(
      builder: (context, provider, child) {
        return Padding(
          padding: EdgeInsets.only(left: width * 0.1),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _ItemWidget(title: 'Name', value: provider.profileModel?.name ?? ''),
              SizedBox(height: height * 0.02),
              _ItemWidget(title: 'Email', value: provider.profileModel?.email ?? ''),
              SizedBox(height: height * 0.02),
              _ItemWidget(title: 'Mobile No', value: provider.profileModel?.phone ?? ''),
              SizedBox(height: height * 0.02),
              _ItemWidget(title: 'Address', value: provider.profileModel?.country ?? ''),
              SizedBox(height: height * 0.02),
              _ItemWidget(title: 'Pincode', value: provider.profileModel?.country ?? ''),
              SizedBox(height: height * 0.02),
            ],
          ),
        );
      }
    );
    // );
  }
}

class _ItemWidget extends StatelessWidget {
  const _ItemWidget({Key? key, required this.title, required this.value}) : super(key: key);
  final String title;
  final String value;

  @override
  Widget build(BuildContext context) {
    final textScale = MediaQuery.of(context).textScaleFactor * 1.2;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(title, textScaleFactor: textScale, style: const TextStyle(color: Colors.white70, fontSize: 17)),
        const SizedBox(height: 10),
        Text(value, textScaleFactor: textScale, style: const TextStyle(color: Colors.white30, fontSize: 12)),
      ],
    );
  }
}
