import 'package:eatiano_app/model/profile/profile_model.dart';
import 'package:eatiano_app/screens/cart/cart_page.dart';
import 'package:eatiano_app/screens/profile/widgets/edit_profile.dart';
import 'package:eatiano_app/screens/profile/widgets/profile_details.dart';
import 'package:eatiano_app/screens/login/login_page.dart';
import 'package:eatiano_app/utils/locator.dart';
import 'package:eatiano_app/utils/session_manager.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:eatiano_app/screens/profile/widgets/notifyBell.dart';
import '../../providers/login_provider.dart';
import '../../repository/profile_provider.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  ProfilePageState createState() => ProfilePageState();
}

class ProfilePageState extends State<ProfilePage> {
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      Provider.of<ProfileProvider>(context, listen: false).fetchData();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    final textScale = MediaQuery.of(context).textScaleFactor * 1.2;
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          elevation: 0,
          centerTitle: true,
          title: Text('Profile', textScaleFactor: textScale),
          actions: [
            InkWell(
              onTap: () => Navigator.of(context).pushNamed(CartPage.routeName),
              child: const Icon(Icons.shopping_cart_outlined, color: Colors.grey),
            ),
            NotifyBell()
          ],
        ),
        body: Selector<ProfileProvider, bool>(
            selector: (c, p) => p.isLoading,
            builder: (context, isLoading, child) {
              return isLoading
                  ? const Center(child: CircularProgressIndicator(color: Colors.red))
                  : Center(
                      child: ListView(
                        children: [
                          CircleAvatar(radius: 50, child: Image.asset('assets/images/NoPath - Copy (14).png')),
                          const SizedBox(height: 10),
                          Align(
                            alignment: Alignment.center,
                            child: TextButton.icon(
                              onPressed: () => context.read<ProfileProvider>().setEdit(),
                              icon: const Icon(Icons.edit, color: Colors.red),
                              label: Text('Edit Profile', textScaleFactor: textScale, style: const TextStyle(color: Colors.red)),
                            ),
                          ),
                          Selector<ProfileProvider, ProfileModel?>(
                              selector: (c, p) => p.profileModel,
                              builder: (context, model, child) {
                                return Center(
                                  child: Padding(
                                    padding: EdgeInsets.only(left: width * 0.04),
                                    child: Text(
                                      'Hi there ${model?.name ?? ''}',
                                      textScaleFactor: textScale,
                                      style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                );
                              }),
                          Align(
                            alignment: Alignment.center,
                            child: TextButton(
                              onPressed: () {
                                context.read<LoginProvider>().logout().then((value) {
                                  if (value['message'] == 'Successfully logged out') {
                                    locator.get<SessionManager>().clearPref();
                                    Navigator.pushNamedAndRemoveUntil(context, LoginPage.routeName, (route) => false);
                                  } else if(value['message'] == 'Unauthenticated.') {
                                    locator.get<SessionManager>().clearPref();
                                    Navigator.pushNamedAndRemoveUntil(context, LoginPage.routeName, (route) => false);
                                  }
                                });
                              },
                              child: Text(
                                'Sign Out',
                                textScaleFactor: textScale,
                                style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 12),
                              ),
                            ),
                          ),
                          SizedBox(height: height * 0.01),
                          Selector<ProfileProvider, bool>(
                              builder: (context, v, c) => v ? const EditProfile() : const ProfileDetails(), selector: (c, p) => p.isEdit),
                        ],
                      ),
                    );
            }));
  }
}
