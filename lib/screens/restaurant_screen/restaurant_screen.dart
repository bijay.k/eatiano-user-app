import 'package:eatiano_app/model/restaurant_model.dart';
import 'package:flutter/material.dart';
import '../../widgets/restaurant/cart.dart';
import '../../widgets/restaurant/notifyBell.dart';
import './pageViewScreen.dart';

class RestaurantScreen extends StatelessWidget {
  static const String routeName = '/restaurants-screen';

  const RestaurantScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final restaurant = ModalRoute.of(context)!.settings.arguments as Restaurant;

    final id = restaurant.restaurantId;
    // final type = restaurant.["type"];
    final numberOfRatings = restaurant.restaurantRatingCount;
    final distance = restaurant.distance;

    double time = distance! / 40;
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    bool tabLayout = width > 600;
    bool largeLayout = width > 350 && width < 600;

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(elevation: 0, iconTheme: const IconThemeData(color: Colors.red), backgroundColor: Colors.transparent),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          SizedBox(
            height: height * 0.44,
            child: Stack(
              children: [
                SizedBox(
                  width: double.infinity,
                  height: height * 0.4,
                  child: Image.network('http://13.127.95.127/eatianoBackend${restaurant.restaurantImage}',
                      errorBuilder: (context, value, stackTrace) => Container(), fit: BoxFit.cover),
                ),
                Positioned(
                  bottom: 10,
                  right: 10,
                  child: CircleAvatar(
                    radius: tabLayout
                        ? 52
                        : largeLayout
                            ? 35
                            : 24,
                    backgroundColor: Colors.white,
                    child: CircleAvatar(
                      radius: tabLayout
                          ? 50
                          : largeLayout
                              ? 33
                              : 22,
                      backgroundColor: const Color.fromRGBO(161, 218, 46, 1),
                      child: Text(restaurant.restaurantRating ?? '', style: const TextStyle(color: Colors.black, fontSize: 25)),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
              width: double.infinity,
              height: tabLayout
                  ? height * 0.1
                  : largeLayout
                      ? height * 0.07
                      : height * 0.1,
              padding: EdgeInsets.symmetric(horizontal: width * 0.02),
              child: Row(
                children: [
                  Flexible(
                    flex: 1,
                    fit: FlexFit.tight,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          restaurant.restaurantName ?? '',
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: tabLayout
                                  ? 35
                                  : largeLayout
                                      ? 25
                                      : 18),
                        ),
                        // Text(
                        //   type ?? '',
                        //   textScaleFactor: textScale,
                        //   style: TextStyle(
                        //       color: Colors.grey,
                        //       fontWeight: FontWeight.bold,
                        //       fontSize: tabLayout
                        //           ? 20
                        //           : largeLayout
                        //               ? 12
                        //               : 11),
                        // ),
                      ],
                    ),
                  ),
                  Flexible(
                    flex: 1,
                    fit: FlexFit.loose,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [Cart(), NotifyBell()],
                            ),
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              )),
          SizedBox(
            height: tabLayout
                ? height * 0.2
                : largeLayout
                    ? height * 0.1
                    : height * 0.085,
            child: Row(
              children: [
                Flexible(
                  flex: 1,
                  fit: FlexFit.loose,
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/images/Icon ionic-md-time (2).png',
                          height: tabLayout
                              ? 60
                              : largeLayout
                                  ? 40
                                  : 30,
                          width: tabLayout
                              ? 60
                              : largeLayout
                                  ? 40
                                  : 30,
                        ),
                        Text(
                          '${time.toStringAsFixed(1)} hr',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: tabLayout
                                  ? 15
                                  : largeLayout
                                      ? 11
                                      : 8),
                        )
                      ],
                    ),
                  ),
                ),
                //  'assets/images/Icon material-location-on.png',
                //double.parse(distance.toStringAsFixed(1))
                Flexible(
                  flex: 1,
                  fit: FlexFit.loose,
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/images/Icon material-location-on.png',
                          height: tabLayout
                              ? 60
                              : largeLayout
                                  ? 40
                                  : 30,
                          width: tabLayout
                              ? 60
                              : largeLayout
                                  ? 40
                                  : 30,
                        ),
                        Text(
                          '${double.parse(distance.toStringAsFixed(1))} km',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: tabLayout
                                  ? 15
                                  : largeLayout
                                      ? 11
                                      : 8),
                        )
                      ],
                    ),
                  ),
                ),
                Flexible(
                  flex: 1,
                  fit: FlexFit.loose,
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/images/bbq (2).png',
                          height: tabLayout
                              ? 60
                              : largeLayout
                                  ? 40
                                  : 30,
                          width: tabLayout
                              ? 60
                              : largeLayout
                                  ? 40
                                  : 30,
                        ),
                        Text(
                          'Delivery',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: tabLayout
                                  ? 15
                                  : largeLayout
                                      ? 11
                                      : 8),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: width * 0.02, vertical: 10),
              child: Column(
                children: [
                  Text(
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Tincidunt dui ut ornare lectus sit amet est. Luctus venenatis lectus magna fringilla urna porttitor rhoncus.',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: tabLayout
                            ? 18
                            : largeLayout
                                ? 12
                                : 10),
                  ),
                  const SizedBox(height: 20),
                  Row(
                    children: [
                      Flexible(
                        flex: 1,
                        child: Center(
                          child: Text(
                            'Details',
                            style: TextStyle(
                                color: Colors.red,
                                // const Color.fromRGBO(161, 218, 46, 1),
                                fontWeight: FontWeight.bold,
                                fontSize: tabLayout
                                    ? 25
                                    : largeLayout
                                        ? 18
                                        : 15,
                                shadows: const [Shadow(color: Colors.grey, blurRadius: 10, offset: Offset(0, 2))]),
                          ),
                        ),
                      ),
                      Flexible(
                        flex: 1,
                        child: InkWell(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute<void>(
                                builder: (BuildContext context) => PageViewScreen(id ?? 0, restaurant.restaurantName!, '', restaurant.restaurantRating!,
                                    restaurant.restaurantImage!, numberOfRatings ?? '')));
                            // Navigator.of(context)
                            //     .pushNamed('/page-view-screen', restaurant: {
                            //   'id': id,
                            //   'name': name,
                            //   'type': type,
                            //   'rating': rating,
                            //   'image': image,
                            //   'numberOfRatings': numberOfRatings
                            // });
                            // Provider.of<RestaurantProductProvider>(context,
                            //         listen: false)
                            //     .fetchCategory(id.toString());
                          },
                          child: Center(
                            child: Text(
                              'Menu',
                              style: TextStyle(
                                  color: const Color.fromRGBO(161, 218, 46, 1),
                                  fontWeight: FontWeight.bold,
                                  fontSize: tabLayout
                                      ? 25
                                      : largeLayout
                                          ? 18
                                          : 15,
                                  shadows: const [Shadow(color: Colors.grey, blurRadius: 10, offset: Offset(0, 2))]),
                            ),
                          ),
                        ),
                      ),
                      Flexible(
                        flex: 1,
                        child: InkWell(
                          onTap: () => Navigator.of(context).pushNamed('/review-screen', arguments: {
                            'id': id,
                            'name': restaurant.restaurantName,
                            'type': 'type',
                            'rating': restaurant.restaurantRating,
                            'image': restaurant.restaurantImage,
                          }),
                          child: Center(
                            child: Text(
                              'Review',
                              style: TextStyle(
                                  color: const Color.fromRGBO(161, 218, 46, 1),
                                  fontWeight: FontWeight.bold,
                                  fontSize: tabLayout
                                      ? 25
                                      : largeLayout
                                          ? 18
                                          : 15,
                                  shadows: const [Shadow(color: Colors.grey, blurRadius: 10, offset: Offset(0, 2))]),
                            ),
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
