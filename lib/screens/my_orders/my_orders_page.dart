import 'package:flutter/material.dart';

import '../../widgets/orders/completedOrders.dart';
import '../../widgets/orders/cooking_tab.dart';

class MyOrdersPage extends StatefulWidget {
  static const String routeName = '/my-order-screen';

  const MyOrdersPage({Key? key}) : super(key: key);

  @override
  MyOrdersPageState createState() => MyOrdersPageState();
}

class MyOrdersPageState extends State<MyOrdersPage> with SingleTickerProviderStateMixin {
  late TabController tabController;

  @override
  void initState() {
    super.initState();
    tabController = TabController(length: 2, vsync: this, initialIndex: 0);
    tabController.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        centerTitle: true,
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        leading: InkWell(onTap: () => Navigator.of(context).pop(), child: const Icon(Icons.arrow_back_ios, color: Colors.red)),
        title: const Text('My Orders', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
        bottom: TabBar(
          controller: tabController,
          indicatorColor: Colors.white,
          tabs: const [Tab(text: 'Cooking'), Tab(text: 'Completed Orders')],
        ),
      ),
      body: TabBarView(
        controller: tabController,
        children: const [CookingTab(), CompletedOrders()],
      ),
    );
  }
}
