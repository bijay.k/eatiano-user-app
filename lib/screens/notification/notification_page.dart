import 'package:flutter/material.dart';
import 'notification_item.dart';

class NotificationsPage extends StatefulWidget {


  const NotificationsPage({Key? key}) : super(key: key);

  @override
  State<NotificationsPage> createState() => _NotificationsPageState();
}

class _NotificationsPageState extends State<NotificationsPage> {
  Map<String, dynamic> _notification = {};

  void _generateData() {
    _notification = {
      'data': [
        {'id': '1', 'message': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', 'date': '16th Nov'},
        {
          'id': '2',
          // 'subject': 'Promotions',
          'message': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
          'date': '16th Nov'
        },
        {
          'id': '3',
          // 'subject': 'Promotions',
          'message': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
          'date': '16th Nov'
        },
        {
          'id': '4',
          // 'subject': 'Promotions',
          'message': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
          'date': '16th Nov'
        },
        {
          'id': '5',
          // 'subject': 'Promotions',
          'message': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
          'date': '16th Nov'
        },
        {
          'id': '6',
          // 'subject': 'Promotions',
          'message': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
          'date': '16th Nov'
        },
        {
          'id': '7',
          // 'subject': 'Promotions',
          'message': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
          'date': '16th Nov'
        },
        {
          'id': '8',
          // 'subject': 'Promotions',
          'message': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
          'date': '16th Nov'
        },
        {
          'id': '9',
          // 'subject': 'Promotions',
          'message': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
          'date': '16th Nov'
        },
        {
          'id': '10',
          // 'subject': 'Promotions',
          'message': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
          'date': '16th Nov'
        },
      ]
    };
  }

  @override
  void initState() {
    _generateData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          elevation: 0,
          centerTitle: true,
          leading: InkWell(onTap: () => Navigator.of(context).pop(), child: const Icon(Icons.arrow_back_ios, color: Colors.red)),
          title: const Text('Notifications', style: TextStyle(color: Colors.white)),
        ),
        body: Container(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          decoration: const BoxDecoration(image: DecorationImage(image: AssetImage('assets/images/Hand_drawn.png'), fit: BoxFit.cover)),
          child: ListView.builder(
            itemBuilder: (context, index) => NotificationItem(
              _notification['data'][index]['message'],
              _notification['data'][index]['date'],
            ),
            itemCount: _notification['data'].length,
          ),
        ));
  }
}
