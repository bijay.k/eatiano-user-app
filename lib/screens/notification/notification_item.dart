import 'package:flutter/material.dart';

class NotificationItem extends StatelessWidget {
  const NotificationItem(this.message, this.date, {Key? key}) : super(key: key);
  final String message;
  final String date;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              const CircleAvatar(radius: 5, backgroundColor: Colors.red),
              const SizedBox(width: 10),
              Text(message, style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 12)),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20),
            child: Text(date, style: const TextStyle(color: Colors.grey, fontSize: 12)),
          )
        ],
      ),
    );
  }
}
