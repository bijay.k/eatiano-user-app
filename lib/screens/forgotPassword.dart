import 'package:eatiano_app/screens/login/widgets/button.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/login_provider.dart';
import 'dart:convert';

class ForgotPassword extends StatefulWidget {
  static const String routeName = '/forgot-password';

  const ForgotPassword({Key? key}) : super(key: key);

  @override
  ForgotPasswordState createState() => ForgotPasswordState();
}

class ForgotPasswordState extends State<ForgotPassword> {
  final _forgotKey = GlobalKey<FormState>();
  var recoveryEmail = '';

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    var textScale = MediaQuery.of(context).textScaleFactor * 1.2;
    bool tabLayout = width > 600;
    bool largeLayout = width > 350 && width < 600;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        leading: InkWell(
            onTap: () => Navigator.of(context).pop(), borderRadius: BorderRadius.circular(30), child: const Icon(Icons.arrow_back_ios, color: Colors.red)),
      ),
      body: Container(
        decoration: const BoxDecoration(image: DecorationImage(image: AssetImage('assets/images/Hand_drawn.png'), fit: BoxFit.cover)),
        child: Form(
          key: _forgotKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Enter Email',
                  textScaleFactor: textScale,
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: tabLayout
                          ? 20
                          : largeLayout
                              ? 16
                              : 12)),
              SizedBox(height: height * 0.02),
              Padding(
                padding: tabLayout ? EdgeInsets.only(left: width * 0.08, right: width * 0.08) : EdgeInsets.only(left: width * 0.04, right: width * 0.04),
                child: TextFormField(
                  decoration: InputDecoration(
                    label: Text('Your Email', style: TextStyle(fontSize: tabLayout ? 18 : 16)),
                    border: OutlineInputBorder(borderRadius: BorderRadius.circular(30.0)),
                    fillColor: Colors.white,
                    filled: true,
                    focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(30.0), borderSide: const BorderSide(color: Colors.white)),
                  ),
                  keyboardType: TextInputType.emailAddress,
                  textInputAction: TextInputAction.next,
                  validator: (email) {
                    if (email!.isEmpty) {
                      return 'Please Enter Email';
                    } else {
                      recoveryEmail = email;
                      return null;
                    }
                  },
                ),
              ),
              SizedBox(height: height * 0.03),
              Button('Request OTP', onPressed: () {
                if (_forgotKey.currentState!.validate()) {
                  _forgotPassword(context);
                }
              },),
              SizedBox(height: height * 0.02),
              Text('Please Check Your Email For The OTP',
                  textScaleFactor: textScale, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: tabLayout ? 14 : 12))
            ],
          ),
        ),
      ),
    );
  }

  void _forgotPassword(BuildContext context) async {
    final data = {'email': recoveryEmail};
    print('Email ${data['email']}');
    // var provider = await Provider.of<Network>(context, listen: false)
    //     .authData(data, 'api/auth/forget_password');
    var provider = await Provider.of<LoginProvider>(context, listen: false).authData(data, 'api/auth/forget_password');

    print(json.decode(provider.body));
    var body = json.decode(provider.body);

    if (body['staus'] == 'success') {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(body['data'].toString(), style: const TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
          action: SnackBarAction(label: 'Close', onPressed: () => ScaffoldMessenger.of(context).hideCurrentSnackBar())));
      Navigator.of(context).pushReplacementNamed('/otp-screen', arguments: {'email': recoveryEmail});
    }
  }
}
