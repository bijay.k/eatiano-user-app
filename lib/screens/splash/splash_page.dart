import 'dart:async';
import 'package:eatiano_app/screens/login/login_page.dart';
import 'package:eatiano_app/utils/locator.dart';
import 'package:eatiano_app/utils/session_manager.dart';
import 'package:eatiano_app/widgets/home/bottom_navigation.dart';
import 'package:flutter/material.dart';

class SplashPage extends StatefulWidget {
  static const String routeName = '/';
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  bool isAuth = false;

  void _checkIfLoggedIn() async {
    String? token = locator.get<SessionManager>().getToken();
    Navigator.pushNamedAndRemoveUntil(context, token.isNotEmpty ? BottomNavigation.routeName : LoginPage.routeName, (route) => false);
  }

  @override
  void initState() {
    Timer(const Duration(milliseconds: 600), () => _checkIfLoggedIn());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.sizeOf(context).height;
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(image: DecorationImage(image: AssetImage('assets/images/bg_splash.png'), fit: BoxFit.cover)),
        child: Stack(
          children: [
            Positioned(
              left: 0,
              right: 0,
              height: height * 0.7 + 50,
              child: Container(
                decoration: const BoxDecoration(color: Colors.orange, borderRadius: BorderRadius.vertical(bottom: Radius.circular(100))),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    const SizedBox(height: 30),
                    TweenAnimationBuilder<double>(
                      duration: const Duration(milliseconds: 300),
                      tween: Tween<double>(begin: 0.0, end: 1.0),
                      builder: (context, value, child) {
                        return Transform(
                          alignment: Alignment.center,
                          transform: Matrix4.identity()..scale(value),
                          child: Opacity(opacity: value, child: Image.asset('assets/images/icon_splash.png', height: height * 0.5)),
                        );
                      },
                    ),
                    TweenAnimationBuilder<double>(
                        duration: const Duration(milliseconds: 400),
                        tween: Tween<double>(begin: 0.0, end: 1.0),
                        builder: (context, value, child) {
                          return Transform.scale(
                              scale: value,
                              alignment: Alignment.center,
                              child: const Text('Welcome to', textAlign: TextAlign.center, style: TextStyle(color: Colors.white, fontSize: 25)));
                        }),
                    TweenAnimationBuilder<double>(
                        duration: const Duration(milliseconds: 500),
                        tween: Tween<double>(begin: 0.0, end: 1.0),
                        builder: (context, value, child) {
                          return Transform.scale(
                            scale: value,
                            alignment: Alignment.center,
                            child: Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 50.0),
                              child: Opacity(
                                  opacity: value,
                                  child: const FittedBox(child: Text('Eatiano', style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600)))),
                            ),
                          );
                        }),
                  ],
                ),
              ),
            ),
            Positioned(
                top: height * 0.7,
                left: 0,
                right: 0,
                child: Container(
                    padding: const EdgeInsets.all(8),
                    decoration: const BoxDecoration(color: Colors.orange, shape: BoxShape.circle),
                    child: Container(
                        padding: const EdgeInsets.all(10),
                        decoration: const BoxDecoration(color: Colors.white, shape: BoxShape.circle),
                        child: Image.asset('assets/images/icon_splash_1.png', height: 60, width: 60))))
          ],
        ),
      ),
    );
  }
}
