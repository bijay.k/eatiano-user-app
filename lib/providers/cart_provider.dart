import 'package:eatiano_app/api_client/api_client.dart';
import 'package:eatiano_app/model/cart_model.dart';
import 'package:eatiano_app/utils/locator.dart';
import 'package:eatiano_app/utils/session_manager.dart';
import 'package:eatiano_app/utils/urls.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class CartItemProvider with ChangeNotifier {
  final _session = locator.get<SessionManager>();
  final _baseService = locator.get<BaseHttpService>();
  List<dynamic> _cartItems = [];

  // Map<String, dynamic> _individualItems = {};
  final List<dynamic> _cartItemList = [];
  List<dynamic> _individualItems = [];

  // List<CartItem> _restaurantTotal = [];
  bool isLoading = false;
  String baseUrl = 'http://13.127.95.127/eatianoBackend/public/index.php/';

  List<CartModel> cartList = [];

  // double deliveryCost = 40;
  double deliveryCost = 0.0;
  double discountCost = 50;

  void setLoading(bool val) {
    isLoading = val;
    notifyListeners();
  }

  void clear() {
    cartList = [];
    isLoading = false;
  }

  List<dynamic> get cartItemList {
    return [..._cartItemList];
  }

  List<dynamic> get cartItems {
    return [..._cartItems];
  }

  Future<void> fetchCartItems() async {
    setLoading(true);
    final headers = {'Authorization': _session.getToken(), 'Accept': 'application/json'};
    CustomRequest request = CustomRequest(url: Urls.cartListUrl, urlName: 'cartList', headers: headers);
    _baseService.onGetRequest(request).then((response) {
      _cartItems = response.result;
      cartList = (response.result as List).map((e) => CartModel.fromJson(e)).toList();
    }).whenComplete(() => setLoading(false));
  }

  Future<void> deleteCartItems(String id) async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    final url = Uri.parse(baseUrl + 'api/auth/cart/$id');
    await http.delete(url, headers: {'Authorization': 'Bearer ${localStorage.getString('token')}', 'Accept': 'application/json'});
    fetchCartItems();
    notifyListeners();
  }

  Future<void> addItems(int id, int quantity, String restaurantId) async {
    setLoading(true);
    final headers = {'Authorization': _session.getToken(), 'Accept': 'application/json'};
    CustomRequest request = CustomRequest(url: Urls.cartListUrl, urlName: 'addToCart', headers: headers, body: {
      'product_id': id.toString(),
      'restaurant_id': restaurantId,
      'quantity': quantity.toString(),
    });
    _baseService.onPostRequest(request).then((response) {
    }).whenComplete(() => setLoading(false));
    fetchCartItems();
  }

  // String id
  void deleteItems(int id) {
    // _cartItems.removeWhere((element) => element.id == id);
    // _individualItems.removeWhere((element) => element.id == id);
    notifyListeners();
  }

  // List<dynamic> getProductById(String id) {
  //   return _cartItems.where((element) => element.id == id).toList();
  // }

  // List<CartItem> getProductById(String id) {
  //   _individualItems = _cartItems.where((element) => element.id == id).toList();
  //   return _individualItems;
  // }

  // List<dynamic> getRestaurantTotal(String id) {
  //   _restaurantTotal = _cartItems.where((element) => element.id == id).toList();
  // }

  double get itemTotal {
    double total = 0.0;
    // cartItems.forEach(
    //     (value) => total += double.parse(value.price) * value.quantity);
    return total;
  }

  // double get itemAmount {
  //   double total = 0.0;
  //   _cartItems.forEach((value) => total += value.price * value.quantity);
  //   return total;
  // }
  double get itemAmount {
    double total = 0.0;
    // _cartItems['data'].forEach((key, value) => total +=
    //     double.parse(value['product_selling_price']) *
    //         double.parse(value['quantity']));
    // return total;
    // total =
    // _cartItems['data'].fold(
    //     0,
    //     (price, value) =>
    //         price +
    //         (double.parse(value['product_selling_price']) *
    //             double.parse(value['quantity'])));
    total = _cartItems.fold(0, (price, value) => price + (double.parse(value['product_selling_price']) * double.parse(value['quantity'])));
    return total;
  }

  double get totalAmount {
    double total = 0.0;
    _individualItems.forEach((value) => total += double.parse(value['data']['product_selling_price']) * double.parse(value['data']['quantity']));
    // notifyListeners();
    return deliveryCost + total;
  }

  double get checkOutAmount {
    double total = 0.0;
    _individualItems.forEach((value) => total += value.price * value.quantity);
    // return (total + deliveryCost) - discountCost;
    return total - discountCost;
  }
}

// print(network.getToken());
// print(restaurantName);
// if (!_cartItems.any((element) => element.id == id)) {
// if (_cartItems.contains(restaurantName) == restaurantName) {
// if (_cartItems.contains(restaurantName)) {
// _cartItems.add(CartItem(
//     id: id,
//     name: name,
//     restaurantName: restaurantName,
//     price: double.parse(price),
//     quantity: quantity.toDouble(),
//     image: image,
//     rating: rating,
//     totalRatings: totalRatings));
// } else {
//   _cartItems.clear();
//   _cartItems.add(CartItem(
//       id: id,
//       name: name,
//       restaurantName: restaurantName,
//       price: double.parse(price),
//       quantity: quantity.toDouble(),
//       image: image,
//       rating: rating,
//       totalRatings: totalRatings));
// notifyListeners();
// }
