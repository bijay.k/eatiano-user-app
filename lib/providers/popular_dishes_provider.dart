import 'dart:convert';

import 'package:eatiano_app/api_client/api_client.dart';
import 'package:eatiano_app/utils/locator.dart';
import 'package:eatiano_app/utils/session_manager.dart';
import 'package:eatiano_app/utils/urls.dart';
import 'package:flutter/material.dart';
import '../model/popular_dishes_model.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class PopularDishesProvider with ChangeNotifier {
  final _session = locator.get<SessionManager>();
  final _service = locator.get<BaseHttpService>();
  Map<String, dynamic> _popularDishes = {};
  Map<String, dynamic> _favouriteDishes = {};
  Map<String, dynamic> _fetchDishes = {};
  List<dynamic> _searchDish = [];
  List<PopularDishes> popularDishList = [];
  List<PopularDishes> mainPopularDishList = [];
  String baseUrl = 'http://13.127.95.127/eatianoBackend/public/index.php/';
  final bool _favourite = false;
  bool isLoading = false;


  void setLoading(bool val) {
    isLoading = val;
    notifyListeners();
  }

  bool get favourite {
    return _favourite;
  }

  Map<String, dynamic> get popularDishes {
    return {..._popularDishes};
  }

  Map<String, dynamic> get favouriteDishes {
    return {..._favouriteDishes};
  }

  List<dynamic> get searchDish {
    return [..._searchDish];
  }

  void sortByCity(String city) {
    popularDishList = mainPopularDishList.where((element) => (element.restaurantAddress ?? '').contains(city)).toList();
    notifyListeners();
  }

  Future<void> fetchData() async {
    setLoading(true);
    final header = {'Authorization': _session.getToken(), 'Accept': 'application/json'};
    CustomRequest request = CustomRequest(url: Urls.allProductsUrl, urlName: 'allProduct', headers: header);
    CustomResponse response = await _service.onGetRequest(request);
    PopularDishesModel popularDishes = PopularDishesModel.fromJson(response.result);
    mainPopularDishList = popularDishes.data;
    popularDishList = popularDishes.data;
    _popularDishes = popularDishes.toJson();
    setLoading(false);
  }

  Future<void> searchFoodData() async {
    final url = Uri.parse(baseUrl + 'api/all_products');
    final response = await http.get(url);
    PopularDishesModel popularDishes = PopularDishesModel.fromJson(jsonDecode(response.body));
    _fetchDishes = popularDishes.toJson();
    _fetchDishes['data'].forEach((value) => _searchDish.add(value));
  }

  Future<void> fetchFavouriteData() async {
    final headers = {'Authorization': _session.getToken(), 'Accept': 'application/json'};
    CustomRequest request = CustomRequest(url: Urls.allProductsUrl, urlName: 'allProduct', headers: headers);
    CustomResponse response = await _service.onGetRequest(request);
    mainPopularDishList = PopularDishesModel.fromJson(response.result).data;
    popularDishList = PopularDishesModel.fromJson(response.result).data;
  }

  dynamic postFavouriteData(String productId, String restaurantId) async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    final url = Uri.parse(baseUrl + 'api/auth/wishlist');
    final response = await http.post(url,
        body: {'product_id': productId, 'restaurant_id': restaurantId},
        headers: {'Authorization': 'Bearer ${localStorage.getString('token')}', 'Accept': 'application/json'});
    return response;
  }

  Future<void> deleteFavouriteData(String productId) async {
    final headers = {'Authorization': _session.getToken(), 'Accept': 'application/json'};
    CustomRequest request = CustomRequest(url: '${Urls.deleteWishlistUrl}/$productId', urlName: 'deleteWishlist', headers: headers);
    await _service.onDeleteRequest(request);
    fetchFavouriteData();
    notifyListeners();
  }
}