import 'package:eatiano_app/api_client/api_client.dart';
import 'package:eatiano_app/model/track_model.dart';
import 'package:eatiano_app/utils/locator.dart';
import 'package:eatiano_app/utils/session_manager.dart';
import 'package:eatiano_app/utils/urls.dart';
import 'package:flutter/material.dart';

class TrackingProvider extends ChangeNotifier {
  final _session = locator.get<SessionManager>();
  final _service = locator.get<BaseHttpService>();
  bool isLoading = true;
  TrackModel? trackModel;

  void setLoading(bool val) {
    isLoading = val;
    notifyListeners();
  }

  final List<OrderStatus> orderStatusList = [
    OrderStatus(status: 'Order assigned to pickup warehouse', icon: 'assets/images/icon_default_dish.png', text: "Order Confirmed"),
    OrderStatus(status: 'Pickup executive assigned', icon: 'assets/images/icon_default_dish.png', text: 'Pickup Executive Assigned'),
    OrderStatus(status: 'Order picked up', icon: 'assets/images/icon_default_dish.png', text: 'Picked By Courier'),
    OrderStatus(status: 'Order shipped', icon: 'assets/images/icon_default_dish.png', text: 'Order Shipped'),
    OrderStatus(status: 'Order has reached destination warehouse', icon: 'assets/images/icon_default_dish.png', text: 'Reached Destination'),
    OrderStatus(status: 'Out for delivery', icon: 'assets/images/icon_default_dish.png', text: 'Out For Delivery'),
    OrderStatus(status: 'delivered', icon: 'assets/images/icon_default_dish.png', text: 'Delivered'),
  ];

  void fetchTracking(int? orderId) {
    setLoading(true);
    Map<String, String> headers = {'Authorization': _session.getToken()};
    CustomRequest request = CustomRequest(url: '${Urls.trackingUrl}$orderId', urlName: 'tracking', headers: headers);
    _service.onGetRequest(request).then((value) {
      trackModel = TrackModel.fromJson(value.result);
    }).whenComplete(() => setLoading(false));
  }
}

class OrderStatus {
  String status;
  String icon;
  String text;

  OrderStatus({required this.status, required this.icon, required this.text});
}
