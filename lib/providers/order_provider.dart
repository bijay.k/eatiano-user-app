import 'package:eatiano_app/api_client/api_client.dart';
import 'package:eatiano_app/model/order_model.dart';
import 'package:eatiano_app/utils/locator.dart';
import 'package:eatiano_app/utils/session_manager.dart';
import 'package:eatiano_app/utils/urls.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import '../model/payment/orderId/orderIdModel.dart';
import 'dart:convert';

class OrderProvider with ChangeNotifier {
  final _session = locator.get<SessionManager>();
  final _service = locator.get<BaseHttpService>();
  String baseUrl = 'http://13.127.95.127/eatianoBackend/public/index.php/';
  Map<String, dynamic> _orderId = {};
  List<Order> orderList = [];
  OrderId? orderId;


  // List<dynamic> _orderId = [];

  // List<dynamic> get orderId {
  //   return [..._orderId];
  // }

  Future<Map<String, dynamic>> getOrderId(Map<String, dynamic>  data, String couponCode) async {

    final header = {'Authorization': _session.getToken(), 'Accept': 'application/json'};
    CustomRequest request = CustomRequest(url: Urls.generateOrderIdUrl, urlName: 'generateOrderId', headers: header, body: data);
    await _service.onPostRequest(request).then((value)  {
      orderId = OrderIdModel.fromJson(value.result).data;
    }).whenComplete(() => notifyListeners());


    // SharedPreferences localStorage = await SharedPreferences.getInstance();
    // final url = Uri.parse(baseUrl + 'api/auth/order_id');
    // final response = await http.post(url,
    //     body: {'state': state, 'coupon_code': couponCode}, headers: {'Authorization': 'Bearer ${localStorage.getString('token')}', 'Accept': 'application/json'});
    //
    // var res = json.decode(response.body);
    //
    // var jsonData = jsonDecode(response.body);
    // OrderId fetchOrderId = OrderId.fromJson(jsonData['data']);
    // _orderId = fetchOrderId.toJson();
    //
    // print(_orderId);

    return _orderId;
    // return _orderId;
    // res['data'].map((element) => _orderId.add(element)).toList();
    // // res['data'].forEach((key, value) => _orderId.add(value));
    // // _orderId = ;
    // print(' $_orderId');

    // OrderId idOrder = orderIdFromJson(res['data'].toString());
    // _orderId = idOrder.toJson();
    // print(_orderId);
  }

  void fetchOrders() async {
    final header = {'Authorization': _session.getToken(), 'Accept': 'application/json'};
    CustomRequest request = CustomRequest(url: Urls.ordersUrl, urlName: 'order', headers: header);
    await _service.onGetRequest(request).then((value)  {
      orderList = OrderModel.fromJson(value.result).data ?? [];
    }).whenComplete(() => notifyListeners());

  }

  Future<CustomResponse> paymentValidation(Map<String, dynamic> body) async {
    print('_session.getToken() : ${_session.getToken()}');
    final header = {'Authorization': _session.getToken(), 'Accept': 'application/json'};
    CustomRequest request = CustomRequest(url: Urls.paymentValidationUrl, urlName: 'order', headers: header, body: body);
    return await _service.onPostRequest(request);
  }

  Future<CustomResponse> updateOrder(Map<String, dynamic> body) async {
    print('_session.getToken() : ${_session.getToken()}');
    final header = {'Authorization': _session.getToken(), 'Accept': 'application/json'};
    CustomRequest request = CustomRequest(url: Urls.ordersUrl, urlName: 'order', headers: header, body: body);
    return await _service.onPostRequest(request);
  }
}
