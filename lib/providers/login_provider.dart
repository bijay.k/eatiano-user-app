import 'package:eatiano_app/api_client/api_client.dart';
import 'package:eatiano_app/model/login_model.dart';
import 'package:eatiano_app/utils/locator.dart';
import 'package:eatiano_app/utils/session_manager.dart';
import 'package:eatiano_app/utils/urls.dart';
import 'package:eatiano_app/widgets/home/bottom_navigation.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginProvider with ChangeNotifier {
  final _session = locator.get<SessionManager>();
  final _service = locator.get<BaseHttpService>();
  String url = 'http://13.127.95.127/eatianoBackend/public/index.php/';
  var token = '';
  var _authToken = '';
  bool isLoading = false;

  void setLoading(bool val) {
    isLoading =  val;
    notifyListeners();
  }

  Future<String?> get authToken async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    _authToken = localStorage.getString('token')!;
    return _authToken;
  }

  getToken() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    // token = jsonDecode(localStorage.getString('token') ?? '')['access_token'];
    // token = jsonDecode(localStorage.getString('token') ?? '')['token'];
    token = localStorage.getString('token') ?? '';
    // _authToken = localStorage.getString('token')!;
    // print('Token $token');
  }

  void login(BuildContext context, String email, String password, String? fcm) async {
    setLoading(true);
    final data = {'email': email, 'password': password, 'fcm_code': fcm};
    Response res = await post(Uri.parse(Urls.loginUrl), body: jsonEncode(data), headers: setHeaders());
    if (res.statusCode == 401) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: const Text('Email or Password invalid', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
        backgroundColor: Colors.white,
        action: SnackBarAction(label: 'Close', onPressed: () => ScaffoldMessenger.of(context).hideCurrentSnackBar()),
      ));
    } else {
      LoginModel model = LoginModel.fromJson(jsonDecode(res.body));
      SharedPreferences localStorage = await SharedPreferences.getInstance();
      localStorage.setString('token', model.accessToken!);
      _session.setAccessToken('Bearer ${model.accessToken}');
      Navigator.of(context).pushReplacementNamed(BottomNavigation.routeName);
      getToken();
    }
    setLoading(false);
  }

  Future<Response> authData(data, apiUrl) async {
    return await http.post(Uri.parse(Urls.loginUrl), body: jsonEncode(data), headers: setHeaders());
  }

  signUp(data, apiUrl) async {
    var fullUrl = url + apiUrl;
    return await http.post(Uri.parse(fullUrl), body: data, headers: {'Accept': 'application/json'});
  }

  facebookSignUp(data, apiUrl) async {
    var fullUrl = url + apiUrl;
    return await http.post(Uri.parse(fullUrl), body: data, headers: {'Accept': 'application/json'});
  }

  googleSignUp(data, apiUrl) async {
    var fullUrl = url + apiUrl;
    return await http.post(Uri.parse(fullUrl), body: data, headers: {'Accept': 'application/json'});
  }

  forgotPassword(data, apiUrl) async {
    var fullUrl = url + apiUrl;
    return await http.post(Uri.parse(fullUrl), body: data, headers: setHeader());
  }

  getData(apiUrl) async {
    var fullUrl = url + apiUrl;
    await getToken();
    return await http.get(Uri.parse(fullUrl), headers: setHeaders());
  }

  setHeaders() => {'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': 'Bearer $token'};

  setHeader() => {
        'Authorization': 'Bearer $token',
        'Accept': 'application/json',
      };

  Future<Map<String, dynamic>> logout() async {
    final header = {'Authorization': _session.getToken(), 'Accept': 'application/json'};
    CustomRequest request = CustomRequest(url: Urls.logoutUrl, urlName: 'logout', headers: header);
    return (await _service.onPostRequest(request)).result;
  }

// checkIfLoggedIn() async {
//   SharedPreferences localStorage = await SharedPreferences.getInstance();
//   token = localStorage.getString('token')!;
//   if (token != '') {
//     _isAuth = true;
//   }
//   return _isAuth ? true : false;
// }
}
