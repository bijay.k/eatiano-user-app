import 'package:eatiano_app/providers/order_provider.dart';
import 'package:eatiano_app/screens/tracking/tracking_page.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CookingTab extends StatefulWidget {
  const CookingTab({Key? key}) : super(key: key);

  @override
  CookingTabState createState() => CookingTabState();
}

class CookingTabState extends State<CookingTab> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      context.read<OrderProvider>().fetchOrders();
    });
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    final textScale = MediaQuery.of(context).textScaleFactor * 1.2;
    return Container(
      padding: EdgeInsets.only(left: width * 0.05, right: width * 0.05, top: 15),
      child: Consumer<OrderProvider>(builder: (context, provider, child) {
        return ListView.builder(
          shrinkWrap: true,
          itemCount: provider.orderList.length,
          itemBuilder: (context, index) {
            final cooking = provider.orderList.elementAt(index);
            return InkWell(
              onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => TrackingPage(order: cooking))),
              child: Container(
                margin: const EdgeInsets.only(bottom: 10),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                        margin: EdgeInsets.only(bottom: height * 0.01),
                        child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                          Container(
                            height: height * 0.08,
                            width: height * 0.08,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10), border: Border.all(color: const Color.fromRGBO(197, 223, 54, 1), width: 2)),
                            child: Image.asset('assets/images/icon_default_dish.png'),
                          ),
                          const SizedBox(width: 10),
                          Flexible(
                            child: Container(
                              padding: EdgeInsets.only(left: width * 0.02),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(cooking.productName ?? '',
                                          textScaleFactor: textScale, style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
                                      Row(
                                        children: [
                                          Text('₹', textScaleFactor: textScale, style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
                                          const SizedBox(width: 5),
                                          Text(cooking.totalAmount.toString() ?? '',
                                              textScaleFactor: textScale, style: const TextStyle(color: Colors.red, fontWeight: FontWeight.bold))
                                        ],
                                      )
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      const Icon(Icons.star_rate_rounded, color: Colors.red),
                                      Text(cooking.totalAmount.toString(), textScaleFactor: textScale, style: const TextStyle(color: Colors.red)),
                                      Text(' (${cooking.totalAmount.toString()} ratings)',
                                          textScaleFactor: textScale, style: const TextStyle(color: Colors.white, fontSize: 12))
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      Text(cooking.productName ?? '', style: const TextStyle(color: Colors.white, fontSize: 12)),
                                      const SizedBox(width: 10),
                                      Text(cooking.productName ?? '', style: const TextStyle(color: Colors.white, fontSize: 12)),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          )
                        ])),
                    // const SizedBox(height: 5),
                    Center(
                        child: Text(cooking.orderStatus ?? '',
                            textScaleFactor: textScale, style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold))),
                    const SizedBox(height: 10),
                    Container(padding: EdgeInsets.only(left: width * 0.1, right: width * 0.1), height: 1, color: const Color.fromRGBO(204, 218, 123, 1))
                  ],
                ),
              ),
            );
          },
        );
      }),
    );
  }
}
