import 'package:eatiano_app/model/popularRestaurants/popularRestaurantProvider.dart';
import 'package:eatiano_app/model/restaurant_model.dart';
import 'package:eatiano_app/screens/restaurant_screen/restaurant_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class RestaurantList extends StatelessWidget {
  const RestaurantList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    bool tabLayout = width > 600;
    bool largeLayout = width > 350 && width < 600;
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        centerTitle: true,
        elevation: 0,
        leading: InkWell(onTap: () => Navigator.of(context).pop(), child: const Icon(Icons.arrow_back_ios, color: Colors.red)),
        title: const Text('Popular Restaurants'),
      ),
      body: Selector<PopularRestaurantProvider, bool>(
          selector: (c, p) => p.isLoading,
          builder: (context, isLoading, child) {
            return isLoading
                ? const Center(child: CircularProgressIndicator(color: Colors.red))
                : Selector<PopularRestaurantProvider, List<Restaurant>>(
                    selector: (c, p) => p.restaurantList,
                    builder: (context, restaurantList, child) {
                      return ListView.builder(
                        padding: const EdgeInsets.all(8.0),
                        itemCount: restaurantList.length,
                        itemBuilder: (context, index) {
                          final response = restaurantList.elementAt(index);
                          return InkWell(
                            onTap: () => Navigator.of(context).pushNamed(RestaurantScreen.routeName, arguments: response),
                            child: Container(
                              height: height * 0.15,
                              margin: EdgeInsets.only(bottom: height * 0.03),
                              padding: EdgeInsets.only(left: width * 0.01, right: width * 0.01),
                              child: Row(
                                children: [
                                  Container(
                                    width: width * 0.35,
                                    decoration: BoxDecoration(
                                      border: Border.all(color: Colors.green, width: 1),
                                      borderRadius: BorderRadius.circular(10),
                                      boxShadow: const [BoxShadow(color: Colors.black, blurRadius: 8, offset: Offset(1, 2))],
                                    ),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(10),
                                      child: Image.network(
                                        'http://13.127.95.127/eatianoBackend${response.restaurantImage}',
                                        errorBuilder: (context, value, stackTrace) => Container(),
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: width * 0.02),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          response.restaurantName ?? '',
                                          style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(height: height * 0.005),
                                        Row(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            const Icon(Icons.map, color: Colors.green),
                                            const SizedBox(width: 5),
                                            Expanded(
                                              child: Text(
                                                response.restaurantAddress ?? '',
                                                style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 11),
                                              ),
                                            ),
                                          ],
                                        ),
                                        SizedBox(height: height * 0.005),
                                        Row(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            const Icon(Icons.location_on, color: Colors.green),
                                            const SizedBox(width: 5),
                                            Expanded(
                                              child: Text(
                                                '${response.distance} Kms',
                                                // overflow: TextOverflow.ellipsis,
                                                style: const TextStyle(
                                                    color: Colors.white,
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 11),
                                              ),
                                            ),
                                          ],
                                        ),
                                        SizedBox(height: height * 0.001),
                                        Row(
                                          children: [
                                            const Icon(Icons.star, color: Colors.yellow),
                                            Text(
                                              response.restaurantRating ?? '',
                                              style: const TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 13),
                                            ),
                                            SizedBox(width: width * 0.01),
                                            Text(
                                              '(${response.restaurantRatingCount ?? ''} Ratings)',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: tabLayout
                                                      ? 18
                                                      : largeLayout
                                                          ? 15
                                                          : 13),
                                            )
                                          ],
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          );
                        },
                      );
                    });
          }),
    );
  }
}
