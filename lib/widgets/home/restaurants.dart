import 'package:eatiano_app/model/restaurant_model.dart';
import 'package:eatiano_app/screens/restaurant_screen/restaurant_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../model/popularRestaurants/popularRestaurantProvider.dart';

class Restaurants extends StatelessWidget {
  const Restaurants({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    bool tabLayout = width > 600;
    bool largeLayout = width > 350 && width < 600;
    return Selector<PopularRestaurantProvider, bool>(
        selector: (c, p) => p.isLoading,
        builder: (context, value, child) {
          return value
              ? const Center(child: CircularProgressIndicator(color: Colors.red))
              : Selector<PopularRestaurantProvider, List<Restaurant>>(
                  selector: (c, p) => p.restaurantList,
                  builder: (context, restaurantList, child) {
                    return Container(
                      padding: EdgeInsets.only(top: height * 0.01, left: 0.05),
                      margin: EdgeInsets.only(bottom: height * 0.05),
                      child: ListView.builder(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemCount: restaurantList.length > 3 ? 3 : restaurantList.length,
                        itemBuilder: (context, index) {
                          final response = restaurantList.elementAt(index);
                          return InkWell(
                            onTap: () => Navigator.of(context).pushNamed(RestaurantScreen.routeName, arguments: response),
                            child: Container(
                              margin: EdgeInsets.only(bottom: height * 0.02),
                              child: Row(
                                children: [
                                  Container(
                                    width: width * 0.3,
                                    height: height * 0.12,
                                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(20), boxShadow: [
                                      BoxShadow(
                                          color: Colors.black,
                                          blurRadius: tabLayout
                                              ? 10
                                              : largeLayout
                                                  ? 5
                                                  : 8,
                                          offset: tabLayout && largeLayout ? const Offset(2, 1) : const Offset(4, 2))
                                    ]),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(tabLayout
                                          ? 20
                                          : largeLayout
                                              ? 20
                                              : 30),
                                      child: Image.network(
                                        'http://13.127.95.127/eatianoBackend${response.restaurantImage}',
                                        errorBuilder: (context, value, stackTrace) => Container(),
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: width * 0.03),
                                  Container(
                                    padding: EdgeInsets.only(top: tabLayout || largeLayout ? height * 0.022 : height * 0.012),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          response.restaurantName ?? '',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                              fontSize: tabLayout
                                                  ? 30
                                                  : largeLayout
                                                      ? 18
                                                      : 10),
                                        ),
                                        // SizedBox(
                                        //   height: tabLayout ? height : largeLayout ? :
                                        //     height: tabLayout || largeLayout
                                        //         ? height * 0.01
                                        //         : height * 0.005),
                                        Row(
                                          children: [
                                            // Text(
                                            //   'Cafe',
                                            //   textScaleFactor: tabLayout
                                            //       ? textScale * 1.4
                                            //       : largeLayout
                                            //           ? 1.1
                                            //           : 0.8,
                                            //   style: const TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
                                            // ),
                                            SizedBox(width: width * 0.02),
                                            // Text(
                                            //   '.${response["data"][index]["type"]}',
                                            //   textScaleFactor: tabLayout
                                            //       ? textScale * 1.4
                                            //       : largeLayout
                                            //           ? 1.1
                                            //           : 0.8,
                                            //   style: const TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
                                            // ),
                                          ],
                                        ),
                                        SizedBox(height: tabLayout || largeLayout ? height * 0.02 : height * 0.01),
                                        Row(
                                          children: [
                                            Image.asset('assets/images/Path 8561.png'),
                                            SizedBox(width: tabLayout || largeLayout ? width * 0.02 : width * 0.01),
                                            Text(
                                              response.restaurantRating ?? '',
                                              style: const TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
                                            ),
                                            SizedBox(width: tabLayout || largeLayout ? width * 0.02 : width * 0.01),
                                            Text(
                                              '(${response.restaurantRatingCount ?? ''} ratings)',
                                              style: const TextStyle(color: Colors.white),
                                            )
                                          ],
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          );
                        },
                        // itemCount: provider["data"]["data"].length,
                      ),
                    );
                  });
        });
  }
}
