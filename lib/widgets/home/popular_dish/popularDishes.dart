import 'package:eatiano_app/screens/itemDetails.dart';
import 'package:flutter/material.dart';
import './priceWidget.dart';
import 'package:provider/provider.dart';
import '../../../providers/popular_dishes_provider.dart';

class PopularDishes extends StatefulWidget {
  const PopularDishes({Key? key}) : super(key: key);

  @override
  PopularDishesState createState() => PopularDishesState();
}

class PopularDishesState extends State<PopularDishes> {

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    bool tabLayout = width > 600;
    bool largeLayout = width > 350 && width < 600;
    var textScale = MediaQuery.of(context).textScaleFactor * 1.1;
    var subTitleScale = MediaQuery.of(context).textScaleFactor * 1.4;
    return Selector<PopularDishesProvider, bool>(
      selector: (c, p) => p.isLoading,
      builder: (context, _isLoading, value) {
        return _isLoading == true
            ? const Center(child: CircularProgressIndicator(color: Colors.red))
            : SizedBox(
                width: double.infinity,
                height: tabLayout
                    ? height * 0.32
                    : tabLayout
                        ? height * 0.32
                        : height * 0.35,
                child: Consumer<PopularDishesProvider>(
                  builder: (context, provider, child) {
                    return ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: provider.popularDishList.length,
                      itemBuilder: (context, index) {
                        final dish = provider.popularDishList.elementAt(index);
                        return InkWell(
                        onTap: () {
                          // Navigator.of(context).pushNamed('/item-details', arguments: {
                          //   'id': provider["data"][index]["product_id"],
                          //   'restaurantName': provider["data"][index]
                          //       ["restaurant_name"],
                          //   'restaurantId': provider["data"][index]["restaurant_id"],
                          //   'name': provider["data"][index]["product_name"],
                          //   'description': provider["data"][index]
                          //       ["product_description"],
                          //   'image': provider["data"][index]["product_image"],
                          //   'price': provider["data"][index]["product_selling_price"],
                          //   // 'rating': provider["data"][index]["rating"],
                          //   'rating': "4.5",
                          //   // 'totalRatings': provider["data"][index]["totalRatings"]
                          //   'totalRatings': "124",
                          // }
                          // );
                          Navigator.of(context).push(MaterialPageRoute(builder: (_) => ItemDetails(dish.productId.toString())));
                        },
                        child: Container(
                          padding: EdgeInsets.only(top: height * 0.001),
                          height: double.infinity,
                          width: width * 0.42,
                          child: Column(
                            children: [
                              Container(
                                  height: height * 0.2,
                                  width: double.infinity,
                                  child: Container(
                                    margin: EdgeInsets.only(left: width * 0.02, right: width * 0.02),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(20),
                                        boxShadow: const [BoxShadow(color: Colors.black, blurRadius: 5, offset: Offset(5, 2))]),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(20),
                                      child: Image.network(
                                        'http://13.127.95.127/eatianoBackend/public${dish.productImage}',
                                        fit: BoxFit.cover,
                                        errorBuilder: (BuildContext context, value, stack) =>
                                            Image.asset('assets/images/icon_default_dish.png', fit: BoxFit.contain, height: height * 0.08, width: width * 0.25),
                                      ),
                                    ),
                                  )),
                              SizedBox(height: height * 0.012),
                              Expanded(
                                child: Container(
                                  width: double.infinity,
                                  // height: 0.085,
                                  // color: Colors.blue,
                                  padding: EdgeInsets.only(left: width * 0.03),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          Expanded(
                                            child: Text(
                                              dish.productName ?? '',
                                              textScaleFactor: textScale,
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: tabLayout
                                                      ? 16
                                                      : largeLayout
                                                          ? 14
                                                          : 11),
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(height: height * 0.005),
                                      Text(
                                        dish.restaurantName ?? '',
                                        textScaleFactor: subTitleScale,
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontSize: tabLayout
                                                ? 14
                                                : largeLayout
                                                    ? 14
                                                    : 10),
                                      ),
                                      SizedBox(height: height * 0.01),
                                      Row(
                                        children: [
                                          PriceWidget(dish.productSellingPrice ?? ''),
                                          SizedBox(width: width * 0.01),
                                          Image.asset('assets/images/Icon ionic-ios-star.png'),
                                          Text(
                                              dish.productRating ?? '',
                                              textScaleFactor: subTitleScale,
                                              style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: tabLayout ? 12 : 7)),
                                          Text(
                                            '(${dish.productRatingCount})',
                                            textScaleFactor: subTitleScale,
                                            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: tabLayout ? 12 : 7),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ); },

                    );
                  }
                ),
              );
      }
    );
  }
}
