import 'package:eatiano_app/model/coupon/coupo_model.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:provider/provider.dart';
import '../../model/coupon/couponProvider.dart';

class BonusOffer extends StatefulWidget {
  const BonusOffer({Key? key}) : super(key: key);

  @override
  BonusOfferState createState() => BonusOfferState();
}

class BonusOfferState extends State<BonusOffer> {
  int activeIndex = 0;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      Provider.of<CouponProvider>(context, listen: false).fetchCoupons();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    bool tabLayout = width > 600;
    bool largeLayout = width > 350 && width < 600;
    return Selector<CouponProvider, bool>(
        selector: (c, p) => p.isLoading,
        builder: (context, isLoading, value) {
          return isLoading
              ? const Center(child: CircularProgressIndicator(color: Colors.red))
              : Selector<CouponProvider, List<CouponData>>(
                  selector: (c, p) => p.couponList,
                  builder: (context, couponData, value) {
                    return Container(
                      height: tabLayout || largeLayout ? height * 0.35 : height * 0.4,
                      padding: EdgeInsets.only(top: tabLayout || largeLayout ? height * 0.04 : height * 0.06),
                      // color: Colors.red,
                      child: Column(
                        children: [
                          CarouselSlider.builder(
                              itemCount: couponData.length,
                              itemBuilder: (context, index, realIndex) {
                                final coupon = couponData.elementAt(index);
                                return Container(
                                  width: MediaQuery.of(context).size.width * 0.8,
                                  margin: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.06),
                                  decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(20))),
                                  child: ClipRRect(borderRadius: BorderRadius.circular(20), child: Image.network(coupon.couponImage, fit: BoxFit.cover,
                                    errorBuilder: (BuildContext context, value, stack) =>
                                        Image.asset('assets/images/Bonous for You.png', fit: BoxFit.contain),
                                  )),
                                );
                              },
                              options: CarouselOptions(
                                  height: MediaQuery.of(context).size.height * 0.25,
                                  autoPlay: true,
                                  enableInfiniteScroll: false,
                                  enlargeCenterPage: true,
                                  enlargeStrategy: CenterPageEnlargeStrategy.height,
                                  onPageChanged: (index, reason) => setState(() => activeIndex = index))),
                          SizedBox(height: height * 0.02),
                          buildIndicator(couponData.length, context, tabLayout, largeLayout)
                        ],
                      ),
                    );
                  });
        });
  }

  Widget buildIndicator(int _bonus, BuildContext context, bool tabLayout, bool largeLayout) => AnimatedSmoothIndicator(
        activeIndex: activeIndex,
        count: _bonus,
        effect: ExpandingDotsEffect(dotHeight: tabLayout ? 15 : 10, dotWidth: tabLayout ? 15 : 10, activeDotColor: Colors.white, dotColor: Colors.red),
      );
}
