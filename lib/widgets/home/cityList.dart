import 'package:eatiano_app/providers/popular_dishes_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CityList extends StatefulWidget {
  const CityList({Key? key}) : super(key: key);

  @override
  CityListState createState() => CityListState();
}

class CityListState extends State<CityList> {
  final List<_CityModel> _cityList = [];
  final List _images = [
    Image.asset('assets/images/Group 9065.png', fit: BoxFit.fill),
    Image.asset('assets/images/india_gate_delhi.png', fit: BoxFit.fill),
    Image.asset('assets/images/Group 9065.png', fit: BoxFit.fill),
    Image.asset('assets/images/india_gate_delhi.png', fit: BoxFit.fill),
    Image.asset('assets/images/favpng_gateway-of-india-india-gate-coloring-book-drawing.png', fit: BoxFit.fill),
    Image.asset('assets/images/Bangalore Palace copy.png', fit: BoxFit.fill)
  ];

  @override
  void initState() {
    _cityList.add(_CityModel(image: 'assets/images/Group 9065.png', cityName: 'Kolkata'));
    _cityList.add(_CityModel(image: 'assets/images/india_gate_delhi.png', cityName: 'Delhi'));
    _cityList.add(_CityModel(image: 'assets/images/favpng_gateway-of-india-india-gate-coloring-book-drawing.png', cityName: 'Mumbai'));
    _cityList.add(_CityModel(image: 'assets/images/Bangalore Palace copy.png', cityName: 'Bangalore'));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    bool tabLayout = width > 600;
    bool largeLayout = width > 350 && width < 600;
    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.13,
      child: ListView.builder(
        itemCount: _cityList.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) => Container(
          height: double.infinity,
          width: MediaQuery.of(context).size.width * 0.23,
          margin: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.012),
          child: InkWell(
            onTap: () => context.read<PopularDishesProvider>().sortByCity(_cityList.elementAt(index).cityName),
            borderRadius: const BorderRadius.all(Radius.circular(10)),
            child: Column(
              children: [
                Container(
                  height: tabLayout
                      ? MediaQuery.of(context).size.height * 0.12
                      : largeLayout
                          ? MediaQuery.of(context).size.height * 0.08
                          : MediaQuery.of(context).size.height * 0.11,
                  width: double.infinity,
                  padding: const EdgeInsets.all(10),
                  decoration: BoxDecoration(borderRadius: const BorderRadius.all(Radius.circular(10)), border: Border.all(color: Colors.white, width: 2)),
                  child: Image.asset(_cityList.elementAt(index).image, fit: BoxFit.fill),
                ),
                SizedBox(height: MediaQuery.of(context).size.height * 0.005),
                Text(_cityList.elementAt(index).cityName,
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: tabLayout
                            ? 18
                            : largeLayout
                                ? 15
                                : 10))
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _CityModel {
  String cityName;
  String image;

  _CityModel({required this.cityName, required this.image});
}
