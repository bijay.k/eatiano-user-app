import 'package:eatiano_app/model/coupon/couponProvider.dart';
import 'package:eatiano_app/model/popularRestaurants/popularRestaurantProvider.dart';
import 'package:eatiano_app/providers/popular_dishes_provider.dart';
import 'package:eatiano_app/repository/location_service.dart';
import 'package:eatiano_app/screens/dishViewAll.dart';
import 'package:eatiano_app/utils/locator.dart';
import 'package:eatiano_app/widgets/home/restaurantViewAll.dart';
import 'package:flutter/material.dart';
import './cityList.dart';
import 'popular_dish/popularDishes.dart';
import 'momsGenieWidget.dart';
import './tasteBudRelief.dart';
import './bonusOffer.dart';
import './expertChoice.dart';
import './restaurants.dart';
import 'package:provider/provider.dart';

class HomeContent extends StatefulWidget {
  const HomeContent({Key? key}) : super(key: key);

  @override
  HomeContentState createState() => HomeContentState();
}

class HomeContentState extends State<HomeContent> {
  final _location = locator.get<LocationService>().getUserLocation();

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      context.read<CouponProvider>().fetchCoupons();
      context.read<PopularDishesProvider>().fetchData();
      final location = await _location;
      context.read<PopularRestaurantProvider>().fetchRestaurants(location.latitude, location.longitude);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    bool tabLayout = width > 600;
    bool largeLayout = width > 350 && width < 600;

    return Padding(
      padding: EdgeInsets.only(left: width * 0.02, top: height * 0.01, right: width * 0.01),
      child: ListView(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Text('Lorem ipsum dolor sit amet, consectetur',
                  // textScaleFactor: textScale,
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: tabLayout
                          ? 30
                          : largeLayout
                              ? 20
                              : 18)),

              const SizedBox(height: 20),
              Text('City With Foods',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: tabLayout
                          ? 25
                          : largeLayout
                              ? 20
                              : 15)),

              // Row(
              //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //   children: [
              // Container(
              //   child: Row(
              //     children: [
              //       Text('View All',
              // textScaleFactor: textScale,
              //           style: TextStyle(
              //               color: Colors.yellow,
              //               fontWeight: FontWeight.bold,
              //               fontSize: tabLayout
              //                   ? 14
              //                   : largeLayout
              //                       ? 8
              //                       : 6)),
              //       Icon(
              //         Icons.keyboard_arrow_right_outlined,
              //         color: Colors.yellow,
              //         size: tabLayout
              //             ? 30
              //             : largeLayout
              //                 ? 20
              //                 : 18,
              //       )
              //     ],
              //   ),
              // ),
              //   ],
              // ),
            ],
          ),
          const SizedBox(height: 20),
          const CityList(),
          const SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Popular Dishes',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: tabLayout
                          ? 25
                          : largeLayout
                              ? 20
                              : 15)),
              InkWell(
                onTap: () => Navigator.of(context).pushNamed(DishViewAll.routeName),
                child: Row(
                  children: [
                    Text('View All',
                        style: TextStyle(
                            color: Colors.yellow,
                            fontWeight: FontWeight.bold,
                            fontSize: tabLayout
                                ? 14
                                : largeLayout
                                    ? 8
                                    : 6)),
                    Icon(
                      Icons.keyboard_arrow_right_outlined,
                      color: Colors.yellow,
                      size: tabLayout
                          ? 30
                          : largeLayout
                              ? 20
                              : 18,
                    )
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(height: 20),
          const PopularDishes(),
          SizedBox(height: height * 0.04),
          Container(
              // width: double.infinity,
              width: width * 1,
              height: tabLayout || largeLayout ? height * 0.32 : height * 0.375,
              padding: EdgeInsets.only(left: width * 0.02, right: width * 0.02),
              // color: Colors.yellow,
              child: const Center(child: MomsGenieWidget())),
          SizedBox(height: height * 0.05),
          TasteBudRelief(),
          SizedBox(height: height * 0.04),
          Text('Bonus For You',
              // textScaleFactor: textScale,
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: tabLayout
                      ? 25
                      : largeLayout
                          ? 20
                          : 15)),
          const BonusOffer(),
          const SizedBox(height: 10),
          Text('Expert\'s Choice',
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: tabLayout
                      ? 25
                      : largeLayout
                          ? 20
                          : 15)),
          const SizedBox(height: 10),
          const ExpertChoice(),
          const SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Popular Restaurants',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: tabLayout
                          ? 25
                          : largeLayout
                              ? 20
                              : 15)),
              InkWell(
                onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => const RestaurantList())),
                // Navigator.of(context).pushNamed('/restaurant-list'),
                child: Row(
                  children: [
                    Text('View All',
                        style: TextStyle(
                            color: Colors.yellow,
                            fontWeight: FontWeight.bold,
                            fontSize: tabLayout
                                ? 14
                                : largeLayout
                                    ? 8
                                    : 6)),
                    Icon(
                      Icons.keyboard_arrow_right_outlined,
                      color: Colors.yellow,
                      size: tabLayout
                          ? 30
                          : largeLayout
                              ? 20
                              : 18,
                    )
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(height: 10),
          const Restaurants(),
          const SizedBox(height: 10),
        ],
      ),
    );
  }
}
