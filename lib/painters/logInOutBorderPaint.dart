import 'package:flutter/material.dart';

class LogInOutBorderPaint extends CustomPainter {
  LogInOutBorderPaint();

  @override
  void paint(Canvas canvas, Size size) {
    final rectShadow = RRect.fromRectAndRadius(Offset.zero & size, const Radius.circular(1));
    final shadowPaint = Paint()
      ..strokeWidth = 3
      ..style = PaintingStyle.stroke
      ..color = Colors.black
      ..maskFilter = const MaskFilter.blur(BlurStyle.outer, 10);
    canvas.drawRRect(rectShadow, shadowPaint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => true;
}
