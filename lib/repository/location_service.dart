import 'dart:async';
import 'package:location/location.dart';

abstract class LocationService {
  Future<LocationData> getUserLocation();
}

class LocationImpl implements LocationService {
  final _location = Location();

  @override
  Future<LocationData> getUserLocation() async {
    return _location.getLocation();
  }
}