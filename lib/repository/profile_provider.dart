import 'package:eatiano_app/api_client/api_client.dart';
import 'package:eatiano_app/model/profile/profile_model.dart';
import 'package:eatiano_app/utils/locator.dart';
import 'package:eatiano_app/utils/session_manager.dart';
import 'package:eatiano_app/utils/urls.dart';
import 'package:flutter/material.dart';

class ProfileProvider with ChangeNotifier {
  final _session = locator.get<SessionManager>();
  final _service = locator.get<BaseHttpService>();
  bool isLoading = true;
  bool isEdit = false;
  ProfileModel? profileModel;

  void setLoading(bool val) {
    isLoading = val;
    notifyListeners();
  }

  void setEdit() {
    isEdit = !isEdit;
    notifyListeners();
  }

  Future<void> fetchData() async {
    setLoading(true);
    _service
        .onGetRequest(CustomRequest(
          url: Urls.profileUrl,
          urlName: 'profile',
          headers: {'Authorization': _session.getToken(), 'Accept': 'application/json'},
        ))
        .then((value) => profileModel = ProfileModel.fromJson(value.result))
        .whenComplete(() => setLoading(false));
  }

  void editProfile() {
    setLoading(true);
    _service
        .onGetRequest(CustomRequest(
          url: Urls.editProfileUrl,
          urlName: 'profile',
          headers: {'Authorization': _session.getToken(), 'Accept': 'application/json'},
        ))
        .then((value) {})
        .whenComplete(() => setLoading(false));
  }
}
