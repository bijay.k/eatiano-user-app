class LoginModel {
  String? accessToken;
  String? tokenType;
  String? role;
  int? expiresIn;
  String? error;

  LoginModel({
    this.accessToken,
    this.tokenType,
    this.role,
    this.expiresIn,
    this.error,
  });

  factory LoginModel.fromJson(Map<String, dynamic> json) => LoginModel(
    accessToken: json["access_token"],
    tokenType: json["token_type"],
    role: json["role"],
    expiresIn: json["expires_in"],
    error: json["error"],
  );

  Map<String, dynamic> toJson() => {
    "access_token": accessToken,
    "token_type": tokenType,
    "role": role,
    "expires_in": expiresIn,
    "error": error,
  };
}
