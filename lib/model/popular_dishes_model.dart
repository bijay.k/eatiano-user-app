class PopularDishesModel {
  PopularDishesModel({
    required this.status,
    required this.data,
  });

  final String status;
  final List<PopularDishes> data;

  factory PopularDishesModel.fromJson(Map<String, dynamic> json) => PopularDishesModel(
        status: json["status"],
        data: json["data"] == null  ? [] : List<PopularDishes>.from(json["data"].map((x) => PopularDishes.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class PopularDishes {
  PopularDishes({
    required this.productId,
    required this.restaurantId,
    required this.restaurantName,
    required this.restaurantAddress,
    required this.productName,
    required this.productDescription,
    required this.productImage,
    required this.productSellingPrice,
    required this.productStatus,
    required this.productQuantity,
    required this.productRating,
    required this.productRatingCount,
    required this.productSellCount,
  });

  final int productId;
  final int? restaurantId;
  final String? restaurantName;
  final String? restaurantAddress;
  final String? productName;
  final String? productDescription;
  final String? productImage;
  final String? productSellingPrice;
  final String? productStatus;
  final String? productQuantity;
  final String? productRating;
  final dynamic productRatingCount;
  final dynamic productSellCount;

  factory PopularDishes.fromJson(Map<String, dynamic> json) => PopularDishes(
        productId: json["product_id"],
        restaurantId: json["restaurant_id"],
        restaurantName: json["restaurant_name"],
        restaurantAddress: json["restaurant_address"],
        productName: json["product_name"],
        productDescription: json["product_description"],
        productImage: json["product_image"],
        productSellingPrice: json["product_selling_price"],
        productStatus: json["product_status"],
        productQuantity: json["product_quantity"],
        productRating: json["product_rating"],
        productRatingCount: json["product_rating_count"],
        productSellCount: json["product_sell_count"],
      );

  Map<String, dynamic> toJson() => {
        "product_id": productId,
        "restaurant_id": restaurantId,
        "restaurant_name": restaurantName,
        "restaurant_address": restaurantAddress,
        "product_name": productName,
        "product_description": productDescription,
        "product_image": productImage,
        "product_selling_price": productSellingPrice,
        "product_status": productStatus,
        "product_quantity": productQuantity,
        "product_rating": productRating,
        "product_rating_count": productRatingCount,
        "product_sell_count": productSellCount,
      };
}
