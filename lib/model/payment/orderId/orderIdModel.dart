class OrderIdModel {
  OrderId? data;
  int? amount;

  OrderIdModel({
    this.data,
    this.amount,
  });

  factory OrderIdModel.fromJson(Map<String, dynamic> json) => OrderIdModel(
    data: json["data"] == null ? null : OrderId.fromJson(json["data"]),
    amount: json["amount"],
  );

  Map<String, dynamic> toJson() => {
    "data": data?.toJson(),
    "amount": amount,
  };
}

class OrderId {
  String? id;
  String? entity;
  int? amount;
  int? amountPaid;
  int? amountDue;
  String? currency;
  String? receipt;
  String? offerId;
  String? status;
  int? attempts;
  List<dynamic>? notes;
  int? createdAt;

  OrderId({
    this.id,
    this.entity,
    this.amount,
    this.amountPaid,
    this.amountDue,
    this.currency,
    this.receipt,
    this.offerId,
    this.status,
    this.attempts,
    this.notes,
    this.createdAt,
  });

  factory OrderId.fromJson(Map<String, dynamic> json) => OrderId(
    id: json["id"],
    entity: json["entity"],
    amount: json["amount"],
    amountPaid: json["amount_paid"],
    amountDue: json["amount_due"],
    currency: json["currency"],
    receipt: json["receipt"],
    offerId: json["offer_id"],
    status: json["status"],
    attempts: json["attempts"],
    notes: json["notes"] == null ? [] : List<dynamic>.from(json["notes"]!.map((x) => x)),
    createdAt: json["created_at"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "entity": entity,
    "amount": amount,
    "amount_paid": amountPaid,
    "amount_due": amountDue,
    "currency": currency,
    "receipt": receipt,
    "offer_id": offerId,
    "status": status,
    "attempts": attempts,
    "notes": notes == null ? [] : List<dynamic>.from(notes!.map((x) => x)),
    "created_at": createdAt,
  };
}
