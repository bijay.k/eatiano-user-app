class RestaurantModel {
  String? status;
  List<Restaurant>? data;

  RestaurantModel({
    this.status,
    this.data,
  });

  factory RestaurantModel.fromJson(Map<String, dynamic> json) => RestaurantModel(
    status: json["status"],
    data: json["data"] == null ? [] : List<Restaurant>.from(json["data"]!.map((x) => Restaurant.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "data": data == null ? [] : List<dynamic>.from(data!.map((x) => x.toJson())),
  };
}

class Restaurant {
  int? restaurantId;
  String? restaurantName;
  String? restaurantAddress;
  String? restaurantImage;
  String? restaurantMetaDeta;
  String? restaurantAddedBy;
  String? restaurantRating;
  String? restaurantRatingCount;
  String? restaurantPh;
  String? lat;
  String? lng;
  String? createdAt;
  String? updatedAt;
  int? cityId;
  double? distance;

  Restaurant({
    this.restaurantId,
    this.restaurantName,
    this.restaurantAddress,
    this.restaurantImage,
    this.restaurantMetaDeta,
    this.restaurantAddedBy,
    this.restaurantRating,
    this.restaurantRatingCount,
    this.restaurantPh,
    this.lat,
    this.lng,
    this.createdAt,
    this.updatedAt,
    this.cityId,
    this.distance,
  });

  factory Restaurant.fromJson(Map<String, dynamic> json) => Restaurant(
    restaurantId: json["restaurant_id"],
    restaurantName: json["restaurant_name"],
    restaurantAddress: json["restaurant_address"],
    restaurantImage: json["restaurant_image"],
    restaurantMetaDeta: json["restaurant_meta_deta"],
    restaurantAddedBy: json["restaurant_added_by"],
    restaurantRating: json["restaurant_rating"],
    restaurantRatingCount: json["restaurant_rating_count"],
    restaurantPh: json["restaurant_ph"],
    lat: json["lat"],
    lng: json["lng"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
    cityId: json["city_id"],
    distance: json["distance"]?.toDouble(),
  );

  Map<String, dynamic> toJson() => {
    "restaurant_id": restaurantId,
    "restaurant_name": restaurantName,
    "restaurant_address": restaurantAddress,
    "restaurant_image": restaurantImage,
    "restaurant_meta_deta": restaurantMetaDeta,
    "restaurant_added_by": restaurantAddedBy,
    "restaurant_rating": restaurantRating,
    "restaurant_rating_count": restaurantRatingCount,
    "restaurant_ph": restaurantPh,
    "lat": lat,
    "lng": lng,
    "created_at": createdAt,
    "updated_at": updatedAt,
    "city_id": cityId,
    "distance": distance,
  };
}
