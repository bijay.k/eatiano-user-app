class ProfileModel {
  ProfileModel({
    required this.id,
    required this.name,
    required this.email,
    required this.phone,
    required this.role,
    required this.referId,
    required this.refarelId,
    required this.oAuthId,
    required this.fbId,
    required this.country,
    required this.emailVerifiedAt,
    required this.createdAt,
    required this.updatedAt,
  });

  final int id;
  final String name;
  final String email;
  final String phone;
  final String role;
  final dynamic referId;
  final String? refarelId;
  final dynamic oAuthId;
  final dynamic fbId;
  final String? country;
  final dynamic emailVerifiedAt;
  final String? createdAt;
  final String? updatedAt;

  factory ProfileModel.fromJson(Map<String, dynamic> json) => ProfileModel(
        id: json["id"],
        name: json["name"],
        email: json["email"],
        phone: json["phone"],
        role: json["role"],
        referId: json["refer_id"],
        refarelId: json["refarel_id"],
        oAuthId: json["o_auth_id"],
        fbId: json["fb_id"],
        country: json["country"],
        emailVerifiedAt: json["email_verified_at"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "email": email,
        "phone": phone,
        "role": role,
        "refer_id": referId,
        "refarel_id": refarelId,
        "o_auth_id": oAuthId,
        "fb_id": fbId,
        "country": country,
        "email_verified_at": emailVerifiedAt,
        "created_at": createdAt,
        "updated_at": updatedAt,
      };
}
