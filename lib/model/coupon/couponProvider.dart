import 'package:eatiano_app/api_client/api_client.dart';
import 'package:eatiano_app/utils/locator.dart';
import 'package:eatiano_app/utils/session_manager.dart';
import 'package:eatiano_app/utils/urls.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import './coupo_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CouponProvider with ChangeNotifier {
  bool isLoading = false;
  final _session = locator.get<SessionManager>();
  final _baseService = locator.get<BaseHttpService>();
  String baseUrl = 'http://13.127.95.127/eatianoBackend/public/index.php/';
  Map<String, dynamic> _coupons = {};
  Map<String, dynamic> _discount = {};
  List<CouponData> couponList = [];

  void setLoading(bool val) {
    isLoading = val;
    notifyListeners();
  }

  Map<String, dynamic> get coupons {
    return {..._coupons};
  }

  Map<String, dynamic> get discount {
    return {..._discount};
  }

  Future<void> fetchCoupons() async {
    setLoading(true);
    final headers = {'Authorization': _session.getToken(), 'Accept': 'application/json'};
    CustomRequest request = CustomRequest(url: Urls.couponListUrl, urlName: 'cartList', headers: headers);
    _baseService.onGetRequest(request).then((response) {
      print('response.result : ${response.result}');
      CouponModel couponModel = CouponModel.fromJson(response.result);
      _coupons = couponModel.toJson();
      couponList = couponModel.data;
    }).whenComplete(() => setLoading(false));
  }

  void selectedCoupon(String code, double amount) async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    localStorage.setString('couponCode', code);
    localStorage.setDouble('discountAmount', amount);
    _discount = {
      'code': localStorage.getString('couponCode'),
      'amount': localStorage.getDouble('discountAmount')
    };
    print(_discount);
    print(localStorage.getDouble('discountAmount'));
    notifyListeners();
  }

  void deleteCoupon(String code, double amount) async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    localStorage.remove('couponCode');
    localStorage.remove('discountAmount');
    _discount = {'code': '', 'amount': 0};
    notifyListeners();
  }
}
