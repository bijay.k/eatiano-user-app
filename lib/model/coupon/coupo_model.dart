class CouponModel {
  CouponModel({
    required this.data,
  });

  final List<CouponData> data;

  factory CouponModel.fromJson(Map<String, dynamic> json) => CouponModel(
        data: List<CouponData>.from(json["data"].map((x) => CouponData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class CouponData {
  CouponData({
    required this.couponId,
    required this.couponCode,
    required this.condition,
    required this.discount,
    required this.couponImage,
    this.createdAt,
    this.updatedAt,
  });

  final int couponId;
  final String couponCode;
  final String condition;
  final String discount;
  final String couponImage;
  final String? createdAt;
  final String? updatedAt;

  factory CouponData.fromJson(Map<String, dynamic> json) => CouponData(
        couponId: json["coupon_id"],
        couponCode: json["coupon_code"],
        condition: json["condition"],
        discount: json["discount"],
        couponImage: json["coupon_image"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
      );

  Map<String, dynamic> toJson() => {
        "coupon_id": couponId,
        "coupon_code": couponCode,
        "condition": condition,
        "discount": discount,
        "coupon_image": couponImage,
        "created_at": createdAt,
        "updated_at": updatedAt,
      };
}
