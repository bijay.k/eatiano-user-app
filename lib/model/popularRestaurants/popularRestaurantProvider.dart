import 'package:eatiano_app/api_client/api_client.dart';
import 'package:eatiano_app/model/restaurant_model.dart';
import 'package:eatiano_app/utils/locator.dart';
import 'package:eatiano_app/utils/session_manager.dart';
import 'package:eatiano_app/utils/urls.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import './popularRestaurantModel.dart';

class PopularRestaurantProvider with ChangeNotifier {
  final _session = locator.get<SessionManager>();
  final _service = locator.get<BaseHttpService>();
  bool isLoading = false;
  String baseUrl = 'http://13.127.95.127/eatianoBackend/public/index.php/';
  Map<String, dynamic> _temp = {};
  Map<String, dynamic> _searchRestaurants = {};
  List<dynamic> _fetchRestaurants = [];
  List<dynamic> _restaurantsList = [];
  List<Restaurant> restaurantList = [];
  final queryParams = {'lat': '22.5735314', 'lng': '88.4331189'};

  Map<String, dynamic> get searchRestaurants {
    return {..._searchRestaurants};
  }

  List<dynamic> get fetchAllRestaurants {
    return [..._fetchRestaurants];
  }

  void setLoading(bool val) {
    isLoading = val;
    notifyListeners();
  }

  Future<void> fetchRestaurants(double? latitude, double? longitude) async {
    setLoading(true);
    final map = {'lat': '$latitude', 'lng': '$longitude'};
    final header = {'Authorization': _session.getToken(), 'Accept': 'application/json'};
    CustomRequest request = CustomRequest(url: Urls.allRestaurantUrl, urlName: 'allRestaurant', params: map, headers: header);
    _service.onGetRequest(request).then((value) => restaurantList = RestaurantModel.fromJson(value.result).data ?? []).whenComplete(() => setLoading(false));
  }

  Future<void> searchRestaurant() async {
    final url = Uri.parse(baseUrl +
        'api/all_restaurant' +
        '?' +
        // 'lat=${latitude.toString()}' +
        'lat=${queryParams['lat']}' +
        '&' +
        // 'lng=${longitude.toString()}'
        'lng=${queryParams['lng']}');
    final response = await http.get(url);
    PopularRestaurants popularRestaurants = popularRestaurantsFromJson(response.body);
    _temp = popularRestaurants.toJson();
    _restaurantsList = _temp['data'] as List;
    print(_restaurantsList);
  }

// Future<void> searchRestaurants(String query) async {
//   final url = Uri.parse(baseUrl +
//       'api/all_restaurant' +
//       '?' +
//       'lat=${queryParams['lat']}' +
//       '&' +
//       'lng=${queryParams['lng']}');
//   final response = await http.get(url);
//   PopularRestaurants popularRestaurants =
//       popularRestaurantsFromJson(response.body);
//   _searchRestaurants = popularRestaurants
//       .toJson()
//       .map((key, value) => value.where((value) {
//         return value['data']['data'][]
//       }));
// }
}
