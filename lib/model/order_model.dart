// To parse this JSON data, do
//
//     final orderModel = orderModelFromJson(jsonString);

import 'dart:convert';

OrderModel orderModelFromJson(String str) => OrderModel.fromJson(json.decode(str));

String orderModelToJson(OrderModel data) => json.encode(data.toJson());

class OrderModel {
  String? status;
  List<Order>? data;

  OrderModel({
    this.status,
    this.data,
  });

  factory OrderModel.fromJson(Map<String, dynamic> json) => OrderModel(
        status: json["status"],
        data: json["data"] == null ? [] : List<Order>.from(json["data"]!.map((x) => Order.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": data == null ? [] : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class Order {
  int? orderId;
  String? orderUniqueId;
  double? totalAmount;
  String? orderStatus;
  String? deliveryDate;
  String? quantity;
  String? productPrice;
  String? productName;
  String? restaurantName;

  Order({
    this.orderId,
    this.orderUniqueId,
    this.totalAmount,
    this.orderStatus,
    this.deliveryDate,
    this.quantity,
    this.productPrice,
    this.productName,
    this.restaurantName,
  });

  factory Order.fromJson(Map<String, dynamic> json) => Order(
        orderId: json["order_id"],
        orderUniqueId: json["order_unique_id"],
        totalAmount: json["total_amount"] is int
            ? json["total_amount"].toDouble()
            : json["total_amount"] is String
                ? double.parse(json["total_amount"])
                : json["total_amount"],
        orderStatus: json["order_status"],
        deliveryDate: json["delivery_date"],
        quantity: json["quantity"],
        productPrice: json["product_price"],
        productName: json["product_name"],
        restaurantName: json["restaurant_name"],
      );

  Map<String, dynamic> toJson() => {
        "order_id": orderId,
        "order_unique_id": orderUniqueId,
        "total_amount": totalAmount,
        "order_status": orderStatus,
        "delivery_date": deliveryDate,
        "quantity": quantity,
        "product_price": productPrice,
        "product_name": productName,
        "restaurant_name": restaurantName,
      };
}
