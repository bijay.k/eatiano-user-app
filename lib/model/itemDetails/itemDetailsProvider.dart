import 'package:flutter/material.dart';
import './itemDetailsModel.dart';
import 'package:http/http.dart' as http;

class ItemDetailsProvider with ChangeNotifier {
  String baseUrl = 'http://13.127.95.127/eatianoBackend/public/index.php/';
  bool isLoading = false;
  ItemDetail? itemDetail;
  Map<String, dynamic> _itemDetails = {};

  Map<String, dynamic> get itemDetails {
    return {..._itemDetails};
  }

  void setLoading(bool val) {
    isLoading = val;
    notifyListeners();
  }

  void clear() {
    isLoading = false;
    itemDetail = null;
  }

  Future<void> getItemDetails(String productId) async {
    setLoading(true);
    final url = Uri.parse(baseUrl + 'api/all_products/$productId');
    final response = await http.get(url);
    ItemDetailModel itemDetailsJson = itemDetailsFromJson(response.body);
    if(itemDetailsJson.data.isNotEmpty) itemDetail = itemDetailsJson.data.elementAt(0);
    _itemDetails = itemDetailsJson.toJson();
    setLoading(false);
  }
}
