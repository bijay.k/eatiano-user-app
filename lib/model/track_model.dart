class TrackModel {
  String? status;
  String? orderUniqueId;
  String? orderStatus;
  List<Price>? price;
  List<Track>? data;
  String? response;

  TrackModel({
    this.status,
    this.orderUniqueId,
    this.orderStatus,
    this.price,
    this.data,
    this.response,
  });

  factory TrackModel.fromJson(Map<String, dynamic> json) => TrackModel(
    status: json["status"],
    orderUniqueId: json["order_unique_id"],
    orderStatus: json["order_status"],
    price: json["price"] == null ? [] : List<Price>.from(json["price"]!.map((x) => Price.fromJson(x))),
    data: json["data"] == null ? [] : List<Track>.from(json["data"]!.map((x) => Track.fromJson(x))),
    response: json["response"],
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "order_unique_id": orderUniqueId,
    "order_status": orderStatus,
    "price": price == null ? [] : List<dynamic>.from(price!.map((x) => x.toJson())),
    "data": data == null ? [] : List<dynamic>.from(data!.map((x) => x.toJson())),
    "response": response,
  };
}

class Track {
  int? productId;
  String? productName;
  String? productImage;
  String? productDescription;
  String? productPrice;
  String? quantity;

  Track({
    this.productId,
    this.productName,
    this.productImage,
    this.productDescription,
    this.productPrice,
    this.quantity,
  });

  factory Track.fromJson(Map<String, dynamic> json) => Track(
    productId: json["product_id"],
    productName: json["product_name"],
    productImage: json["product_image"],
    productDescription: json["product_description"],
    productPrice: json["product_price"],
    quantity: json["quantity"],
  );

  Map<String, dynamic> toJson() => {
    "product_id": productId,
    "product_name": productName,
    "product_image": productImage,
    "product_description": productDescription,
    "product_price": productPrice,
    "quantity": quantity,
  };
}

class Price {
  String? buyingPrice;
  double? tax;
  String? deliveryCharge;
  String? discountAmount;

  Price({
    this.buyingPrice,
    this.tax,
    this.deliveryCharge,
    this.discountAmount,
  });

  factory Price.fromJson(Map<String, dynamic> json) => Price(
    buyingPrice: json["buying_price"],
    tax: json["tax"] is int ? json["tax"].toDouble() : json["tax"],
    deliveryCharge: json["delivery_charge"],
    discountAmount: json["discount_amount"],
  );

  Map<String, dynamic> toJson() => {
    "buying_price": buyingPrice,
    "tax": tax,
    "delivery_charge": deliveryCharge,
    "discount_amount": discountAmount,
  };
}
