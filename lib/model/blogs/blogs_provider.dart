import 'package:eatiano_app/utils/urls.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import './blogs_model.dart';

class BlogsProvider with ChangeNotifier {
  bool isLoading = true;
  Map<String, dynamic> _blogs = {};

  Map<String, dynamic> get blogs {
    return {..._blogs};
  }

  void setLoading(bool val) {
    isLoading = val;
    notifyListeners();
  }

  Future<void> fetchData() async {
    setLoading(true);
    final url = Uri.parse(Urls.allBlogsUrl);
    final response = await http.get(url);
    Blogs blogs = blogsFromJson(response.body);
    _blogs = blogs.toJson();
    print(_blogs);

    setLoading(false);
  }
}
